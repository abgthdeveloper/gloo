package com.orevon.fflok;

import android.app.Application;

public class AppController extends Application {

//    private User currentUser;
    /**
     * A singleton instance of the application class for easy access in other places
     */
    private static AppController sInstance;

    /**
     * @return co.pixelmatter.meme.ApplicationController singleton instance
     */
    public static synchronized AppController getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // initialize the singleton
        sInstance = this;
    }
}
