package com.orevon.fflok.listeners;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.orevon.fflok.handlers.ShakeEventHandler;

public class ShakeEventListener implements SensorEventListener {

    private static final float ERROR = (float) 7.0;
    private final SensorManager mSensorManager;
    private final Sensor mAccelerometer;
    private Boolean init = false;
    private float lastX, lastY, lastZ;
    private ShakeEventHandler mHandler = null;

    public ShakeEventListener(SensorManager sensorManager, Sensor accelerometer) {
        mSensorManager = sensorManager;
        mAccelerometer = accelerometer;
    }

    public void registerHandler(ShakeEventHandler handler) {
        init = false;
        mHandler = handler;
        mSensorManager.registerListener(this, mAccelerometer,
                SensorManager.SENSOR_DELAY_UI);
    }

    public void unregisterHandler(ShakeEventHandler handler) {
        init = false;
        mHandler = null;
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent e) {
        float x = e.values[0], y = e.values[1], z = e.values[2];

        if (!init) {
            init = true;
            lastX = x;
            lastY = y;
            lastZ = z;
        } else {
            float diffX = Math.abs(lastX - x);
            float diffY = Math.abs(lastY - y);
            float diffZ = Math.abs(lastZ - z);

            // handle noise
            if (diffX < ERROR) {
                diffX = (float) 0.0;
            }
            if (diffY < ERROR) {
                diffY = (float) 0.0;
            }
            if (diffZ < ERROR) {
                diffZ = (float) 0.0;
            }

            lastX = x;
            lastY = y;
            lastZ = z;

            // Horizontal shake?
            if (diffX > diffY) {
                if (mHandler != null) {
                    mHandler.handleShakeEvent();
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}