package com.orevon.fflok.customcontrols;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class HelveticaRegularTextView extends android.support.v7.widget.AppCompatTextView {

    public HelveticaRegularTextView(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public HelveticaRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public HelveticaRegularTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Helvetica-Regular.ttf");
        setTypeface(tf);
    }
}