package com.orevon.fflok.fragments;


import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.orevon.fflok.R;
import com.orevon.fflok.utils.AndroidUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProbabilityFragment extends DialogFragment implements View.OnClickListener  {

    public static ProbabilityFragment newInstance() {
        ProbabilityFragment fragment = new ProbabilityFragment();
        return fragment;
    }

    public ProbabilityFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.MessageDialogTheme);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        AndroidUtils.customizeDialog(dialog);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_probability, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.closebutton).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}
