package com.orevon.fflok.fragments;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.orevon.fflok.R;
import com.orevon.fflok.models.MatchUser;
import com.orevon.fflok.models.User;
import com.orevon.fflok.utils.AndroidUtils;
import com.orevon.fflok.utils.CommonUtils;
import com.orevon.fflok.utils.FflokDataSingleton;
import com.orevon.fflok.utils.internet.NetworkUtils;
import com.orevon.fflok.utils.internet.VolleySingleton;
import com.orevon.fflok.views.ProgressHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.orevon.fflok.constants.ApiPaths.BASEURL;
import static com.orevon.fflok.constants.ApiPaths.GET_RANDOM_USER;
import static com.orevon.fflok.constants.ApiPaths.MATCH_FRIEND;
import static com.orevon.fflok.constants.FflokConstants.X_API_KEY;

public class MatchFragment extends Fragment {

    private ConstraintLayout layout;
    private TextView user1Name, user2Name, vs;
    private ImageView user1ImageView, user2ImageView;
    private int clicks = 0;
    private MatchUser user1, user2;

    public MatchFragment() {
    }

    public static MatchFragment newInstance() {
        return new MatchFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        AndroidUtils.setStatusBarColor(getActivity().getWindow(), getContext(), R.color.lightBlue);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_match, container, false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        AndroidUtils.setStatusBarColor(getActivity().getWindow(), getContext(), android.R.color.white);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        layout = view.findViewById(R.id.layout);
        vs = view.findViewById(R.id.vs);
        user1Name = view.findViewById(R.id.user1Name);
        user2Name = view.findViewById(R.id.user2Name);
        user1ImageView = view.findViewById(R.id.user1Pic);
        user2ImageView = view.findViewById(R.id.user2Pic);

//        user1Name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                clicks++;
//                if (clicks == 1) {
//                    AndroidUtils.setStatusBarColor(getActivity().getWindow(), getContext(), R.color.welcomeGreen);
//                    layout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.welcomeGreen));
//                    vs.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.circle_red_bg));
//                } else if (clicks == 2) {
//                    AndroidUtils.setStatusBarColor(getActivity().getWindow(), getContext(), R.color.coolRed);
//                    layout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.coolRed));
//                    vs.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.circle_green_bg));
//                }
//            }
//        });
        user1Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callMatchFriendRequest(user1.getId());
            }
        });
        user2Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callMatchFriendRequest(user1.getId());
            }
        });
        callGetRandomUserRequest();
    }

    private void callGetRandomUserRequest() {
        ProgressHandler progressHandler = new ProgressHandler(getContext());
        StringRequest request = getRandomFriendsVolleyRequest(progressHandler);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void callMatchFriendRequest(String friendId) {
        ProgressHandler progressHandler = new ProgressHandler(getContext());
        StringRequest request = getMatchFriendVolleyRequest(progressHandler, friendId);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void setMatchUsersData() {
        if(getActivity() != null && isAdded()) {
            if (user1.getPathImage() != null && !user1.getPathImage().equals("")) {
                Glide.with(getActivity())
                        .load(user1.getPathImage()) // Uri of the picture
                        .apply(RequestOptions.circleCropTransform())
                        .into(user1ImageView);
            } else {
                Glide.with(getActivity())
                        .load(R.drawable.logo_white) // Uri of the picture
                        .apply(RequestOptions.circleCropTransform())
                        .into(user1ImageView);
            }
            if (user2.getPathImage() != null && !user2.getPathImage().equals("")) {
                Glide.with(getActivity())
                        .load(user2.getPathImage()) // Uri of the picture
                        .apply(RequestOptions.circleCropTransform())
                        .into(user2ImageView);
            } else {
                Glide.with(getActivity())
                        .load(R.drawable.logo_white) // Uri of the picture
                        .apply(RequestOptions.circleCropTransform())
                        .into(user2ImageView);
            }
            user1Name.setText(user1.getName() != null ? user1.getName() : "");
            user2Name.setText(user2.getName() != null ? user2.getName() : "");

            user1ImageView.setVisibility(View.VISIBLE);
            user2ImageView.setVisibility(View.VISIBLE);
            user1Name.setVisibility(View.VISIBLE);
            user2Name.setVisibility(View.VISIBLE);
            vs.setVisibility(View.VISIBLE);
        }
    }

    private StringRequest getRandomFriendsVolleyRequest(final ProgressHandler progressHandler) {
        return new StringRequest(Request.Method.GET, BASEURL + GET_RANDOM_USER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        progressHandler.hide();
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonarray = jsonResponse.getJSONArray("users");
                            user1 = new Gson().fromJson(jsonarray.get(0).toString(), MatchUser.class);
                            user2 = new Gson().fromJson(jsonarray.get(1).toString(), MatchUser.class);
                            setMatchUsersData();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getContext());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getContext()));
                return headers;
            }
        };
    }

    private StringRequest getMatchFriendVolleyRequest(final ProgressHandler progressHandler, final String idFriend) {
        return new StringRequest(Request.Method.PUT, BASEURL + MATCH_FRIEND,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        try {
                            JSONObject result = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressHandler.hide();
                        callGetRandomUserRequest();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getContext());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("idFriend", idFriend);
                return params;
            }

            //
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getContext()));
                return headers;
            }
        };
    }
}
