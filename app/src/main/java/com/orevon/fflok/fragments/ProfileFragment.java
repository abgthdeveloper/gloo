package com.orevon.fflok.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.orevon.fflok.R;
import com.orevon.fflok.adapters.ProfileListAdapter;
import com.orevon.fflok.adapters.ProfileSpinnerAdapter;
import com.orevon.fflok.models.CheckBoxItem;
import com.orevon.fflok.models.ProfileListItem;
import com.orevon.fflok.models.ProfileSpinnerModel;
import com.orevon.fflok.models.User;
import com.orevon.fflok.utils.CommonUtils;
import com.orevon.fflok.utils.FflokDataSingleton;
import com.orevon.fflok.utils.internet.NetworkUtils;
import com.orevon.fflok.utils.internet.VolleySingleton;
import com.orevon.fflok.views.PeopleImageVideoView;
import com.orevon.fflok.views.ProgressHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.orevon.fflok.constants.ApiPaths.BASEURL;
import static com.orevon.fflok.constants.ApiPaths.BASE_VIDEO_URL;
import static com.orevon.fflok.constants.ApiPaths.GET_USER_PROFILE;
import static com.orevon.fflok.constants.FflokConstants.X_API_KEY;


public class ProfileFragment extends Fragment implements AdapterView.OnItemSelectedListener, ProfileListAdapter.OnItemClicked {

    private List<ProfileListItem> profileItemList = new ArrayList<>();
    private List<ProfileSpinnerModel> spinnerList = new ArrayList<>();
    private RecyclerView recyclerView;
    private View bgContainer;
    private ProfileListAdapter profileListAdapter;
    Spinner spinner;
    private Button confidentialiteButton, comptentButton, effectuesButton, friendMatchButton;
    private TextView profileNameTextView, address;
    private int spinnerSelectedPosition = 0;
    private String videoFilePath;
    private User profileUser;
    private PeopleImageVideoView profileImageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the confidentialite_alert_dialog for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        initViews(view);
        setButtonClickListeners();
        setupProfileListAdapter();
        setupSpinner();
        getProfileDetails();
        return view;
    }

    private void initViews(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_profile);
        bgContainer = view.findViewById(R.id.bg_container);
        confidentialiteButton = view.findViewById(R.id.confidentialiteButton);
        spinner = (Spinner) view.findViewById(R.id.spinnerLayout);
        profileNameTextView = (TextView)view.findViewById(R.id.profile_name);
        address = (TextView)view.findViewById(R.id.profile_address);
        comptentButton = (Button)view.findViewById(R.id.comptent);
        effectuesButton = (Button)view.findViewById(R.id.effectues);
        friendMatchButton = (Button)view.findViewById(R.id.friendMatch);
        profileImageView = (PeopleImageVideoView)view.findViewById(R.id.profileImageView);
    }

    private void setButtonClickListeners() {
       confidentialiteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new ConfidentialiteFragment();
                final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.addToBackStack("ProfileFragment");
                transaction.replace(R.id.fragment_frame, fragment).commit();
            }
        });
    }

    private void setupProfileListAdapter() {
        profileListAdapter = new ProfileListAdapter(profileItemList, getContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(profileListAdapter);
        profileListAdapter.setOnClick(this);
        prepareProfileListData();
    }

    private void prepareProfileListData() {
        List<CheckBoxItem> checkBoxItems = new ArrayList<>();
        CheckBoxItem checkBoxItem = new CheckBoxItem("Ultra connecte", " Connectez 5 reseaux sociaux",false);
        checkBoxItems.add(checkBoxItem);
        checkBoxItem = new CheckBoxItem("Les gens qui comptent vraiment ", " Super ffloker 10 amis",false);
        checkBoxItems.add(checkBoxItem);
        checkBoxItem = new CheckBoxItem("Lelu", " Gagnez un friend match",true);
        checkBoxItems.add(checkBoxItem);
        checkBoxItem = new CheckBoxItem("Gastronome", " Scannez 15 cartes des visites des restaurants",false);
        checkBoxItems.add(checkBoxItem);


        ProfileListItem item = new ProfileListItem("Nouvelle recrue", "Inscrit avec succes", 1, 1, "3 amis ont debloque ce badge", R.color.nouvelle_green, R.drawable.profile_icon1, R.drawable.progress_drawable_green);
        item.setCheckBoxItems(checkBoxItems);
        profileItemList.add(item);

        item = new ProfileListItem("Community builder", "Les gens qui comptent vraiment", 3, 4, "", R.color.community_blue, R.drawable.profile_icon2, R.drawable.progress_drawable_blue);
        item.setCheckBoxItems(checkBoxItems);
        profileItemList.add(item);

        item = new ProfileListItem("Swag", "Bla nlalsjhsdf lksdfjsdfd fs lkjsdfl", 1, 4, "3 amis ont debloque ce badge", R.color.swag_yellow, R.drawable.profile_icon3, R.drawable.progress_drawable_yellow);
        item.setCheckBoxItems(checkBoxItems);
        profileItemList.add(item);

        item = new ProfileListItem("Roi de la Soiree", "Bla nlalsjhsdf lksdfjsdfd fs lkjsdfl", 3, 4, "", R.color.roi_pink, R.drawable.profile_icon4, R.drawable.progress_drawable_pink);
        item.setCheckBoxItems(checkBoxItems);
        profileItemList.add(item);

        item = new ProfileListItem("Fire starter", "Bla nlalsjhsdf lksdfjsdfd fs lkjsdfl", 1, 4, "3 amis ont debloque ce badge", R.color.fire_red, R.drawable.profile_icon5, R.drawable.progress_drawable_red);
        item.setCheckBoxItems(checkBoxItems);
        profileItemList.add(item);

        item = new ProfileListItem("Ambassadeur", "Bla nlalsjhsdf lksdfjsdfd fs lkjsdfl", 0, 4, "", R.color.ambassador_yellow, R.drawable.profile_icon6, R.drawable.progress_drawable_yellow_amb);
        item.setCheckBoxItems(checkBoxItems);
        profileItemList.add(item);

        item = new ProfileListItem("Succes speciale", "Bla nlalsjhsdf lksdfjsdfd fs lkjsdfl", 0, 4, "", R.color.succes_green, R.drawable.star1, R.drawable.progress_drawable_green_success);
        item.setCheckBoxItems(checkBoxItems);
        profileItemList.add(item);

        profileListAdapter.notifyDataSetChanged();
    }

    private void setupSpinner() {
        spinner.setOnItemSelectedListener(this);
        spinnerList = new ArrayList<>();
        ProfileSpinnerModel item = new ProfileSpinnerModel("Nouvelle recrue", R.drawable.profile_icon1, R.drawable.profile_icon1_selected);
        spinnerList.add(item);

        item = new ProfileSpinnerModel("Community builder", R.drawable.profile_icon2, R.drawable.profile_icon2_selected);
        spinnerList.add(item);

        item = new ProfileSpinnerModel("Fire starter", R.drawable.profile_icon5, R.drawable.profile_icon5_selected);
        spinnerList.add(item);

        item = new ProfileSpinnerModel("Ambassadeur", R.drawable.profile_icon6, R.drawable.profile_icon6_selected);
        spinnerList.add(item);

        ProfileSpinnerAdapter adapter = new ProfileSpinnerAdapter(getContext(), spinnerList);
        spinner.setAdapter(adapter);

    }

    private void setProfileData() {
        profileNameTextView.setText(profileUser.getUsername().getText());
//        address.setText(profileUser.getAdresse());
        comptentButton.setText(String.valueOf(profileUser.getLikers().size()));
        effectuesButton.setText(String.valueOf(profileUser.getNumberFflok()));
        if(videoFilePath != null) {
            profileImageView.setData(BASE_VIDEO_URL + videoFilePath, false);
        }
        profileImageView.profileImageView.clearColorFilter();
    }

    private void getProfileDetails() {
        ProgressHandler progressHandler = new ProgressHandler(getContext());
        StringRequest request = getUserProfileRequest(progressHandler);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private StringRequest getUserProfileRequest(final ProgressHandler progressHandler) {
        return new StringRequest(Request.Method.GET, BASEURL + GET_USER_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONObject userJson = jsonResponse.getJSONObject("user");
                            profileUser = new Gson().fromJson(userJson.toString(), User.class);
                            if (profileUser.getPathVideo().length() > 0) {
                                videoFilePath = profileUser.getPathVideo();
                            } else {
                                videoFilePath = null;
                            }
                            setProfileData();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressHandler.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, Objects.requireNonNull(getActivity()).getApplicationContext());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getContext()));
                return headers;
            }
        };
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if(view != null) {
            TextView spinnerText = (TextView) adapterView.findViewById(R.id.spinnerText);
            spinnerText.setTextColor(ContextCompat.getColor(getContext(), R.color.whiteSmoke));

            ImageView dropdownIcon = (ImageView) adapterView.findViewById(R.id.dropdown_icon);
            dropdownIcon.setVisibility(View.VISIBLE);

            ImageView icon = (ImageView) adapterView.findViewById(R.id.image_icon);
            icon.setImageResource(spinnerList.get(i).getSelectedImageId());
            spinnerSelectedPosition = i;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        View view= adapterView.getChildAt(0);
        ImageView dropdownIcon = (ImageView) adapterView.findViewById(R.id.dropdown_icon);
        dropdownIcon.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(int position) {
        boolean isExpanded = !profileItemList.get(position).isExpanded();
        if (isExpanded) {
            for (ProfileListItem item:
                 profileItemList) {
                item.setExpanded(false);
            }
        }
        profileItemList.get(position).setExpanded(isExpanded);
        profileListAdapter.notifyDataSetChanged();
        bgContainer.setBackgroundColor(ContextCompat.getColor(getActivity(), profileItemList.get(position).getColor()));
    }

    @Override
    public void onResume() {
        super.onResume();
        spinner.setSelection(spinnerSelectedPosition);
    }

}
