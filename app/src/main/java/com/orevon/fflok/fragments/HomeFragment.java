package com.orevon.fflok.fragments;


import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.test.mock.MockPackageManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.orevon.fflok.R;
import com.orevon.fflok.activity.ViewFussActivity;
import com.orevon.fflok.listeners.ShakeEventListener;
import com.orevon.fflok.models.Fflok;
import com.orevon.fflok.models.Place;
import com.orevon.fflok.models.ProximateFriend;
import com.orevon.fflok.models.User;
import com.orevon.fflok.utils.CommonUtils;
import com.orevon.fflok.utils.FflokDataSingleton;
import com.orevon.fflok.utils.internet.GPSTracker;
import com.orevon.fflok.utils.internet.NetworkUtils;
import com.orevon.fflok.utils.internet.VolleyMultipartRequest;
import com.orevon.fflok.utils.internet.VolleySingleton;
import com.orevon.fflok.views.PeopleImageVideoView;
import com.orevon.fflok.views.ProgressHandler;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.bclogic.pulsator4droid.library.PulsatorLayout;

import static android.app.Activity.RESULT_OK;
import static com.orevon.fflok.constants.ApiPaths.ACCEPT_FFLOK;
import static com.orevon.fflok.constants.ApiPaths.ADD_USER_STEP4;
import static com.orevon.fflok.constants.ApiPaths.BASEURL;
import static com.orevon.fflok.constants.ApiPaths.BASE_VIDEO_URL;
import static com.orevon.fflok.constants.ApiPaths.CREATE_FFLOK;
import static com.orevon.fflok.constants.ApiPaths.GET_FFLOK_DETAILS;
import static com.orevon.fflok.constants.ApiPaths.GET_PROXIMITE_FRIENDS;
import static com.orevon.fflok.constants.ApiPaths.GET_PROXIMITE_PLACE;
import static com.orevon.fflok.constants.ApiPaths.GET_USER_PROFILE;
import static com.orevon.fflok.constants.FflokConstants.X_API_KEY;

/**
 * Handle Shake (Accelerometer) events
 */
interface ShakeEventHandler {
    void handleShakeEvent();
}

public class HomeFragment extends Fragment implements ShakeEventHandler, PeopleImageVideoView.PeopleImageVideoViewClickedCallBack {

    private static final int CLOSE_POPUP_TIMEOUT = 7000;
    private static final int ACTION_TAKE_VIDEO = 3;
    private static final String VIDEO_STORAGE_KEY = "viewvideo";
    private static final String VIDEOVIEW_VISIBILITY_STORAGE_KEY = "videoviewvisibility";
    private static final int PERMISSIONS_REQUEST_LOCATION_HOME = 3;
    ImageButton plusButton;
    PeopleImageVideoView mVideoView;
    TextView mincePassTextView, timeTextView;
    View mincePassSeparator;
    private float xCoOrdinate;
    ArrayList<ProximateFriend> selectedFriendsList = new ArrayList<ProximateFriend>();
    private ShakeEventListener mShakeListener;
    private ColorMatrixColorFilter filter;
    private ImageView vibrateIcon;
    private PeopleImageVideoView users[];
    private int random1 = 0, random2 = 7;
    private Handler handler = new Handler();
    private View popup;
    private Uri mVideoUri = null;
    private RelativeLayout redbutton;
//    private Runnable changeColorRunnable = new Runnable() {
//        @Override
//        public void run() {
//            randomlyChangeColor();
////            handler.postDelayed(this, 3000);
//        }
//    };
    private TextView title, subTitle, subTitle1;
//    private ImageView user1, user2, user3, user4, user5, user6, user7, user8, user12, user13,user14;
    private String videoFilePath;
    private String latitude, longitude;
    private GPSTracker gps;
    private List<ProximateFriend> proximiteFiends = new ArrayList<>();

    private enum HomePageType  {
            NORMAL, FROM_NOTIFICATION
    }
    private HomePageType homePageType = HomePageType.NORMAL;

    public HomeFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void handleShakeEvent() {
        makePhoneIconVibrate();
        //TODO:commented runnbale -check functionality
//        handler.postDelayed(changeColorRunnable, 0);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        setButtonClickListeners(view);
        setHomePageType(this.homePageType);
        callGetProfileAPI();
    }

    //TODO:call this function when getting fflok notification
    private void setHomePageType(HomePageType type) {
        this.homePageType = type;
        initViewAccordingToPageType(type);
    }

    private void initViews(View view) {
        redbutton = view.findViewById(R.id.superbutton);
        title = view.findViewById(R.id.title);
        subTitle = view.findViewById(R.id.subTitle);
        subTitle1 = view.findViewById(R.id.subTitle1);
        mVideoView = view.findViewById(R.id.videoView1);
        plusButton = view.findViewById(R.id.plusButton);
        popup = view.findViewById(R.id.popup);
        vibrateIcon = view.findViewById(R.id.vibrateIcon);
        mincePassTextView = view.findViewById(R.id.mince_pas_tv);
        timeTextView = view.findViewById(R.id.timeTextView);
        mincePassSeparator = view.findViewById(R.id.mince_pas_separator);
        initPulsator(view);
        initPlusButton(view);
        initPeoplesContainer(view);
        initVibrateIcon(view);
        initUsers(view);
//        initProfileVideoView(view);
    }

    private void setButtonClickListeners(View view) {
        view.findViewById(R.id.mince_pas_tv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new MincePasFragment();
                final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_frame, fragment).commit();
            }
        });

        redbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAcceptFlokAPI();
            }
        });
    }

    private void setTitleSubTitle(HomePageType type) {
        if(type == HomePageType.NORMAL) {
            if(proximiteFiends.size() > 0 ) {
                title.setText(getString(R.string.invite)+ "Teddy");
                subTitle.setText(R.string.home_page_subtitle);
                subTitle1.setText("");
            } else {
                String name = "Hey";
                if(FflokDataSingleton.getInstance().getUser() != null && FflokDataSingleton.getInstance().getUser().getUsername() != null && !FflokDataSingleton.getInstance().getUser().getUsername().getText().equals("")) {
                    name = FflokDataSingleton.getInstance().getUser().getUsername().getText();
                }
                title.setText(String.format(getString(R.string.your_turn), name));
                subTitle.setText("");
                subTitle1.setText("");
            }
        }  else if(type == HomePageType.FROM_NOTIFICATION){
//            title.setText(String.format(getString(R.string.choppe), "Teddy"));
            title.setText(R.string.retour);
            subTitle.setText("");
            subTitle1.setText("");
        }
    }

    private void checkIfLoadedFirstTime() {
        setTitleSubTitle(this.homePageType);
        if(FflokDataSingleton.getInstance().getHomePageLoadedFirstTime(getContext())) {
            FflokDataSingleton.getInstance().setHomePageLoadedFirstTime(false, getContext());
            closePopupAfterTimeout(this.getView());
        }
    }

    private void navigateToMyFflokFragment() {
        Fragment fragment = new MyFflok2Fragment();
        final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.addToBackStack("HomeFragment");
        transaction.replace(R.id.fragment_frame, fragment).commit();
    }

    private void initViewAccordingToPageType(HomePageType type) {
        switch (type) {
            case NORMAL:

                setTitleSubTitle(this.homePageType);
                redbutton.setVisibility(View.GONE);
                mincePassTextView.setVisibility(View.GONE);
                mincePassSeparator.setVisibility(View.GONE);
                timeTextView.setVisibility(View.GONE);
                vibrateIcon.setVisibility(View.GONE);
                getLocationGPSandMakeRequest();

                break;
            case FROM_NOTIFICATION:
                setTitleSubTitle(this.homePageType);
                redbutton.setVisibility(View.VISIBLE);
                mincePassTextView.setVisibility(View.VISIBLE);
                mincePassSeparator.setVisibility(View.VISIBLE);
                timeTextView.setVisibility(View.VISIBLE);
                vibrateIcon.setVisibility(View.GONE);
                callGetFflokDetailsAPI();
                break;
        }
        //general cases
        mVideoView.setVisibility(View.GONE);
        plusButton.setVisibility(View.VISIBLE);
    }

    private void callGetProfileAPI() {
        ProgressHandler progressHandler = new ProgressHandler(getContext());
        StringRequest request = getUserProfileVolleyRequest(progressHandler);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void callGetProximateFriendsAPI() {
        ProgressHandler progressHandler = new ProgressHandler(getContext());
        StringRequest request = getProximiteFriendsVolleyRequest(progressHandler);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getContext())
                .addToRequestQueue(request);
    }

    private void callGetProximatePlacesAPI() {
        ProgressHandler progressHandler = new ProgressHandler(getContext());
        StringRequest request = getProximitePlaceVolleyRequest(progressHandler);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getContext())
                .addToRequestQueue(request);
    }

    private void callCreateFlokAPI() {
        ProgressHandler progressHandler = new ProgressHandler(getContext());
        StringRequest request = createFflokVolleyRequest(progressHandler);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void callAcceptFlokAPI() {
        ProgressHandler progressHandler = new ProgressHandler(getContext());
        StringRequest request = acceptFflokVolleyRequest(progressHandler);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void callGetFflokDetailsAPI() {
        ProgressHandler progressHandler = new ProgressHandler(getContext());
        StringRequest request = getFflokDetailsVolleyRequest(progressHandler);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void initProfileVideoView() {
        if(videoFilePath != null && videoFilePath != "") {
            mVideoView.setVisibility(View.VISIBLE);
            plusButton.setVisibility(View.GONE);
            mVideoView.setData(BASE_VIDEO_URL + videoFilePath, false);
        } else {
            mVideoView.setVisibility(View.GONE);
            plusButton.setVisibility(View.VISIBLE);
        }
//        mVideoView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(videoFilePath == null || videoFilePath.getPath() == "") {
//                    MainBottomSheetDialogFragment fragment = new MainBottomSheetDialogFragment();
//                    fragment.show(getActivity().getSupportFragmentManager(), "123");
//                } else {
//                    //TODO:play video
//                }
//            }
//        });

    }

    private void initPulsator(View view) {
        PulsatorLayout pulsator = view.findViewById(R.id.pulsator);
        pulsator.start();
    }

    private void initPlusButton(View view) {
//        plusButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                closePopupAfterTimeout(view);
//
//            }
//        });

        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(videoFilePath == null || videoFilePath == "") {
                    MainBottomSheetDialogFragment fragment = new MainBottomSheetDialogFragment();
                    fragment.show(getActivity().getSupportFragmentManager(), "123");
                }
            }
        });
    }

    private void closePopupAfterTimeout(final View view) {
        popup.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                popup.setVisibility(View.GONE);
//                MainBottomSheetDialogFragment fragment = new MainBottomSheetDialogFragment();
//                fragment.show(getActivity().getSupportFragmentManager(), "123");
            }
        }, CLOSE_POPUP_TIMEOUT);
    }

    private void initUsers(View view) {
        users = new PeopleImageVideoView[11];
        users[0] = view.findViewById(R.id.user6);
        users[1] = view.findViewById(R.id.user12);
        users[2] = view.findViewById(R.id.user3);
        users[3] = view.findViewById(R.id.user4);
        users[4] = view.findViewById(R.id.user2);
        users[5] = view.findViewById(R.id.user1);
        users[6] = view.findViewById(R.id.user13);
        users[7] = view.findViewById(R.id.user5);
        users[8] = view.findViewById(R.id.user14);
        users[9] = view.findViewById(R.id.user8);
        users[10] = view.findViewById(R.id.user7);

        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        filter = new ColorMatrixColorFilter(matrix);

        for (PeopleImageVideoView iv : users) {
            if (iv != null && iv.profileImageView != null) {
                iv.profileImageView.setColorFilter(filter);
            }
        }
        setFriendsData(new ArrayList<ProximateFriend>());
    }

    private void initVibrateIcon(View view) {
        vibrateIcon.setColorFilter(ResourcesCompat.getColor(getResources(), R.color.textGray, null),
                PorterDuff.Mode.SRC_IN);
        vibrateIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selectedFriendsList.size() > 0) {
                    callCreateFlokAPI();
                } else {
                     Toast.makeText(getContext(), "Please select atleast 1 friend", Toast.LENGTH_SHORT).show();
//                     startActivity(new Intent(getContext(), ViewFussActivity.class));//TODO: comment this
                }
            }
        });
    }

    private void makePhoneIconVibrate() {
        ObjectAnimator animator = ObjectAnimator
                .ofFloat(vibrateIcon, "translationX",
                        0, 25, -25, 25, -25, 15, -15, 6, -6, 0);
        animator.setDuration(1000);
        animator.setRepeatCount(1);
        animator.start();
    }

    private void initPeoplesContainer(View view) {
        RelativeLayout peoplesContainer = view.findViewById(R.id.peoplesContainer);
        peoplesContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                        xCoOrdinate = view.getX() - event.getRawX();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        view.animate().x(event.getRawX() + xCoOrdinate).setDuration(0).start();
                        break;
                    case MotionEvent.ACTION_UP:
                        view.animate().x(0).setDuration(500).start();
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });
    }

//    private void randomlyChangeColor() {
//        random1++;
//        if (random1 > 10)
//            random1 = 0;
//        random2++;
//        if (random2 > 10)
//            random2 = 0;
//
//        changeColor();
//    }

//    private void changeColor() {
//        for (int i = 0; i < 11; i++) {
//            if (i == random1 || i == random2) {
//                users[i].clearColorFilter();
//            } else {
//                users[i].setColorFilter(filter);
//            }
//        }
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // In fragment class callback
        switch (requestCode) {
            case ACTION_TAKE_VIDEO: {
                if (resultCode == RESULT_OK) {
                    Log.e("homefrag", "on activity result in fragment");
                    handleCameraVideo(data);
                }
                break;
            }
        }
    }

    private void handleCameraVideo(Intent intent) {
            mVideoUri = intent.getData();
        mVideoView.setVisibility(View.VISIBLE);
        mVideoView.setData(mVideoUri.getPath(), false);//TODO uncomment
        subTitle1.setText(R.string.main_screen_msg1);
        subTitle.setText("");
//        Glide.with(getActivity())
//                .load(mVideoUri) // Uri of the picture
//                .apply(RequestOptions.circleCropTransform())
//                .into(mVideoView.profileImageView);

        plusButton.setVisibility(View.GONE);
        //TODO:try setting profile image as bitmap at first fraction of video
//        Bitmap bitmap = null;
//        try {
//            bitmap = CommonUtils.retriveVideoFrameFromVideo(mVideoUri.getPath());
//            if (bitmap != null) {
//                bitmap = Bitmap.createScaledBitmap(bitmap, 240, 240, false);
//                mVideoView.profileImageView.setImageBitmap(bitmap);
//            }
//        } catch (Throwable throwable) {
//            throwable.printStackTrace();
//        }
        //or
//        bitmap = ThumbnailUtils.createVideoThumbnail(mVideoUri.getPath(), MediaStore.Images.Thumbnails.MINI_KIND);
//        mVideoView.profileImageView.setImageBitmap(bitmap);
        callGetProximateFriendsAPI();
        ProgressHandler progressHandler = new ProgressHandler(getContext());
        VolleyMultipartRequest request = getAddVideoRequest(mVideoUri, progressHandler);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MenuChooseEvent event) {
        dispatchTakeVideoIntent();
    }

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 5);
        startActivityForResult(takeVideoIntent, ACTION_TAKE_VIDEO);
    }

    //TODO:check function
    private void setFriendsData(List<ProximateFriend> friendList) {
        selectedFriendsList.clear();
        for (int i = 0; i< users.length; i++) {
            if (i < friendList.size()) {
                users[i].setVisibility(View.VISIBLE);
                if (friendList.get(i).getPathImg() != null && !friendList.get(i).getPathImg().equals("")) {
                    users[i].setData(BASE_VIDEO_URL + friendList.get(i).getPathImg(), true);
                }
                users[i].setProximateFriend(friendList.get(i));
                users[i].setCallback(this);
            } else {
                users[i].setVisibility(View.GONE);
            }
        }
    }

    private StringRequest getUserProfileVolleyRequest(final ProgressHandler progressHandler) {
        return new StringRequest(Request.Method.GET, BASEURL + GET_USER_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        progressHandler.hide();
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONObject userJson = jsonResponse.getJSONObject("user");
                            User user = new Gson().fromJson(userJson.toString(), User.class);
                            FflokDataSingleton.getInstance().setUser(user);
                            Activity activity = getActivity();
                            if(activity != null && isAdded()) {//If only attached to fragment

                                checkIfLoadedFirstTime();
                                if(latitude != null && longitude != null && FflokDataSingleton.getInstance().getUser() != null && FflokDataSingleton.getInstance().getUser().getPathVideo() != null && !FflokDataSingleton.getInstance().getUser().getPathVideo().equals("")) {
                                    callGetProximateFriendsAPI();
                                }
                                if (user.getPathVideo().length() > 0) {
                                    videoFilePath = user.getPathVideo();
                                } else {
                                    videoFilePath = null;
                                }
                                initProfileVideoView();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getContext());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getContext()));
                return headers;
            }
        };
    }

    private void getLocationGPSandMakeRequest() {
        try {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != MockPackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST_LOCATION_HOME);
                // If any permission above not allowed by user, this condition will execute every time, else your else part will work
            } else {
                gps = new GPSTracker(getContext());
                // check if GPS enabled
                if(gps.canGetLocation()){

                    latitude = String.valueOf(gps.getLatitude());
                    longitude = String.valueOf(gps.getLongitude());
                    FflokDataSingleton.getInstance().setLatitude(gps.getLatitude());
                    FflokDataSingleton.getInstance().setLongitude(gps.getLongitude());
                    if(FflokDataSingleton.getInstance().getUser() != null && FflokDataSingleton.getInstance().getUser().getPathVideo() != null && !FflokDataSingleton.getInstance().getUser().getPathVideo().equals("")) {
                        callGetProximateFriendsAPI();
                    }
                    callGetProximatePlacesAPI();
                }else{
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_LOCATION_HOME: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocationGPSandMakeRequest();
                } else {
                    // Permission was not granted.
                    Toast.makeText(this.getContext(), R.string.permission_denied_access_location,Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private StringRequest getProximiteFriendsVolleyRequest(final ProgressHandler progressHandler) {
        return new StringRequest(Request.Method.POST, BASEURL + GET_PROXIMITE_FRIENDS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        progressHandler.hide();
                        try {
                            JSONObject result = new JSONObject(response);
                            Boolean success = result.getBoolean("success");
                            Integer friendsCount = result.getInt("number friends");
                            JSONArray jsonarray = result.getJSONArray("friends ");
                            proximiteFiends = new ArrayList<>();
                            for(int i = 0; i < jsonarray.length(); i++) {
                                Gson gson = new Gson();
                                ProximateFriend friend = gson.fromJson(jsonarray.get(i).toString(), ProximateFriend.class);
                                proximiteFiends.add(friend);
                            }
                            setFriendsData(proximiteFiends);
                            setTitleSubTitle(HomeFragment.this.homePageType);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getContext());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("latitude", latitude);
                params.put("longitude", longitude);
                params.put("paging","1"); // 1 or 2
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getContext()));
                return headers;
            }
        };
    }

    private StringRequest getProximitePlaceVolleyRequest(final ProgressHandler progressHandler) {
        return new StringRequest(Request.Method.POST, BASEURL + GET_PROXIMITE_PLACE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        progressHandler.hide();
                        try {
                            JSONObject result = new JSONObject(response);
                            Boolean success = result.getBoolean("success");
                            Integer placesCount = result.getInt("legth places");
                            JSONArray jsonarray = result.getJSONArray("places ");
                            List<Place> proximatePlaces = new ArrayList<>();
                            for(int i = 0; i < jsonarray.length(); i++) {
                                Gson gson = new Gson();
                                Place place = gson.fromJson(jsonarray.get(i).toString(), Place.class);
                                proximatePlaces.add(place);
                            }
                            FflokDataSingleton.getInstance().setProximatePlaces(proximatePlaces);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getContext());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("latitude", latitude);
                params.put("longitude", longitude);
                return params;
            }
            //
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getContext()));
                return headers;
            }
        };
    }

    private VolleyMultipartRequest getAddVideoRequest(final Uri selectedUri, final ProgressHandler progressHandler) {
        return new VolleyMultipartRequest(Request.Method.PUT, BASEURL + ADD_USER_STEP4, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    JSONObject result = new JSONObject(resultResponse);
                    Boolean success = result.getBoolean("success");
                    progressHandler.hide();
//                    JSONArray jsonarray = result.getJSONArray("path");
//                    List<FilePathModel> paths = new ArrayList<>();
//                    for(int i = 0; i < jsonarray.length(); i++) {
//                        Gson gson = new Gson();
//                        FilePathModel path = gson.fromJson(jsonarray.get(i).toString(), FilePathModel.class);
//                        paths.add(path);
//                    }
                    //TODO:check video upload
                    String pathVideoString = result.getString("path");
                    FflokDataSingleton.getInstance().getUser().setPathVideo(pathVideoString);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mVideoView.setData("", false);
                mVideoView.setVisibility(View.GONE);
                plusButton.setVisibility(View.VISIBLE);
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message+" Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message+ " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message+" Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                progressHandler.hide();
                Log.i("Error", errorMessage);
                Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getContext()));
                return headers;


            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                try {
                    InputStream iStream = getContext().getContentResolver().openInputStream(selectedUri);
                    byte[] inputData = CommonUtils.getBytes(iStream);
                    params.put("video", new DataPart("file.mp4", inputData, CommonUtils.getMimeType(selectedUri, getContext())));

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return params;
            }
        };
    }

    private StringRequest createFflokVolleyRequest(final ProgressHandler progressHandler) {
        return new StringRequest(Request.Method.POST, BASEURL + CREATE_FFLOK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        selectedFriendsList.clear();
                        progressHandler.hide();
                        try {
                            JSONObject result = new JSONObject(response);
                            String fflokId = result.getString("idGloo");
                            FflokDataSingleton.getInstance().setFflokId(fflokId);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(new Intent(getContext(), ViewFussActivity.class));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getContext());
                    }
                }
        ) {

//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                HashMap<String, String> params = new HashMap<String, String>();
////                String json = new Gson().toJson(selectedFriendsList);
////                params.put("friends", json);
//                //tODO: check code
//                List<String> friendIds = new ArrayList<>();
//                for (ProximateFriend friend:
//                     selectedFriendsList) {
//                    friendIds.add(friend.getId());
//                }
////                JSONArray json1 = new JSONArray(friendIds);
////                params.put("friends", json1.toString());
//
//
////                String[] array1 = friendIds.toArray(new String[friendIds.size()]);
////                params.put("friends", array1);
//                params.put("create_by", FflokDataSingleton.getInstance().getUserId(getContext()));
//                return params;
//            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getContext()));
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                HashMap<String, Object> params = new HashMap<String, Object>();
                List<String> friendIds = new ArrayList<>();
                for (ProximateFriend friend:
                     selectedFriendsList) {
                    friendIds.add(friend.getId());
                }
//                JSONArray json1 = new JSONArray(friendIds);
//                params.put("friends", json1);
                String[] array1 = friendIds.toArray(new String[friendIds.size()]);
                params.put("friends", array1);
//                params.put("create_by", FflokDataSingleton.getInstance().getUserId(getContext()));
                return new JSONObject(params).toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
    }

    private StringRequest getFflokDetailsVolleyRequest(final ProgressHandler progressHandler) {
        return new StringRequest(Request.Method.GET, BASEURL + GET_FFLOK_DETAILS + FflokDataSingleton.getInstance().getFflokId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        progressHandler.hide();
                        Fflok fflok = new Gson().fromJson(response, Fflok.class);
                        FflokDataSingleton.getInstance().setFflok(fflok);
//                        setFriendsData();
                        //TODO: set fflok data- friends surrounding
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getContext());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("idfflok", FflokDataSingleton.getInstance().getFflokId());
                params.put("idUser", FflokDataSingleton.getInstance().getUserId(getContext()));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getContext()));
                return headers;
            }
        };
    }

    private StringRequest acceptFflokVolleyRequest(final ProgressHandler progressHandler) {
        return new StringRequest(Request.Method.PUT, BASEURL + ACCEPT_FFLOK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        progressHandler.hide();
                        try {
                            JSONObject result = new JSONObject(response);
                            Boolean success = result.getBoolean("success");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        navigateToMyFflokFragment();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getContext());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("idfflok", FflokDataSingleton.getInstance().getFflokId());
                params.put("idUser", FflokDataSingleton.getInstance().getUserId(getContext()));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getContext()));
                return headers;
            }
        };
    }

    public static class MenuChooseEvent {

        public MenuChooseEvent() {

        }
    }

    @Override
    public void peopleImageVideoViewClicked(ProximateFriend friend) {
        if(!selectedFriendsList.contains(friend)) {
            selectedFriendsList.add(friend);
        }
        if(selectedFriendsList.size() > 0 ) {
            vibrateIcon.setVisibility(View.VISIBLE);
        }
    }
}



