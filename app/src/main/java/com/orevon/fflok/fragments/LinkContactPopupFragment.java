package com.orevon.fflok.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.orevon.fflok.R;
import com.orevon.fflok.activity.MainActivity;
import com.orevon.fflok.adapters.ContactRecordAdapter;
import com.orevon.fflok.models.ApiResponse.AddUserFriendResponse;
import com.orevon.fflok.models.ContactRecord;
import com.orevon.fflok.utils.CommonUtils;
import com.orevon.fflok.utils.FflokDataSingleton;
import com.orevon.fflok.utils.internet.NetworkUtils;
import com.orevon.fflok.utils.internet.VolleySingleton;
import com.orevon.fflok.views.ProgressHandler;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.orevon.fflok.constants.ApiPaths.ADD_FRIEND;
import static com.orevon.fflok.constants.ApiPaths.BASEURL;
import static com.orevon.fflok.constants.FflokConstants.X_API_KEY;

public class LinkContactPopupFragment extends DialogFragment {
    private View contactPopupFragment;
    private Button closebutton;
    private RelativeLayout topImageLayout;
    private TextView msg1, msg2, userName, textViewInitials;
    private Button nextButton;
    private ImageView profileImage;

    private int contactCount = 0;
    private ContactRecord contactRecord;
    private AddFriendUpdateCallBack callback;


    public LinkContactPopupFragment() {

    }


    public static LinkContactPopupFragment newInstance() {
        LinkContactPopupFragment fragment = new LinkContactPopupFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.MessageDialogTheme);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().getAttributes().windowAnimations = R.style.MessageDialogTheme;
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        return dialog;
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        contactPopupFragment = inflater.inflate(R.layout.fragment_link_contact_popup, container, false);
        closebutton = contactPopupFragment.findViewById(R.id.close_button1);
        nextButton = contactPopupFragment.findViewById(R.id.next_button1);
        profileImage = contactPopupFragment.findViewById(R.id.user_image1);
        topImageLayout = contactPopupFragment.findViewById(R.id.top_image_layout);
        msg1 = contactPopupFragment.findViewById(R.id.msg1);
        msg2 = contactPopupFragment.findViewById(R.id.msg2);
        userName = contactPopupFragment.findViewById(R.id.user_name);
        profileImage.setImageDrawable(null);
        textViewInitials = contactPopupFragment.findViewById(R.id.textViewInitials);
        textViewInitials.setText(null);
        closebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        setLayouts();
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getActivity().finish();
//                Intent intent = new Intent(getActivity(), MainActivity.class);
//                startActivity(intent);
                ProgressHandler progressHandler = new ProgressHandler(getActivity());
                StringRequest request = addFriendVolleyRequest(progressHandler, contactRecord);
                request.setShouldCache(false);
                VolleySingleton.getInstance(getApplicationContext())
                        .addToRequestQueue(request);
            }
        });
        return contactPopupFragment;
    }

    public void setContactCount(int contactCount) {
        this.contactCount = contactCount;
    }

    public void setContactRecord(ContactRecord contactRecord) {
        this.contactRecord = contactRecord;
    }

    private void setLayouts() {
        if(contactCount > 0) {
            if (contactCount == 1) {
                topImageLayout.setVisibility(View.VISIBLE);
                ColorMatrix matrix = new ColorMatrix();
                matrix.setSaturation(0);
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                profileImage.setColorFilter(filter);
                msg2.setVisibility(View.VISIBLE);
                msg1.setVisibility(View.INVISIBLE);
                if(contactRecord != null) {
                    userName.setText(contactRecord.getName());
                    msg2.setText(String.format(msg2.getText().toString(), contactRecord.getName()));
                    try {

                        if (contactRecord.getThumbnail() != null) {
                            profileImage.setImageDrawable(null);
                            Bitmap bmp = contactRecord.getThumbnail();
                            RoundedBitmapDrawable roundedBmpDrawable =
                                    RoundedBitmapDrawableFactory.create(getResources(), bmp);
                            roundedBmpDrawable.setCircular(true);

                            profileImage.setVisibility(View.VISIBLE);
                            textViewInitials.setVisibility(View.GONE);
                            profileImage.setImageDrawable(roundedBmpDrawable);
                        } else {
                            if (contactRecord.getInitials() != null) {
                                textViewInitials.setText(contactRecord.getInitials());
                                profileImage.setVisibility(View.INVISIBLE);
                                textViewInitials.setVisibility(View.VISIBLE);
                            }
                        }
                    } catch (OutOfMemoryError e) {
                        e.printStackTrace();
                    }
                }
            } else {
                topImageLayout.setVisibility(View.GONE);
                msg2.setVisibility(View.INVISIBLE);
                msg1.setVisibility(View.VISIBLE);
            }
        }
    }

    private StringRequest addFriendVolleyRequest(final ProgressHandler progressHandler, final ContactRecord contactRecord) {
        return new StringRequest(Request.Method.PUT, BASEURL + ADD_FRIEND,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        progressHandler.hide();
                        AddUserFriendResponse addUserFriendResponse = new Gson().fromJson(response, AddUserFriendResponse.class);
                        if(callback != null) {
                            callback.addFriendUpdated(contactRecord);
                        }
                        getActivity().finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getApplicationContext());
                        getActivity().finish();
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("phone",contactRecord.getPhone().replaceAll("\\s+",""));
                String email = contactRecord.getEmail().replaceAll("\\s+","");
                params.put("email", CommonUtils.isValidEmail(email) ? email : "abc@gmail.com");
                params.put("name", contactRecord.getName());
                params.put("source", contactRecord.getSource());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getApplicationContext()));
                return headers;
            }
        };
    }

    public void setCallback(AddFriendUpdateCallBack callback){
        this.callback = callback;
    }
    public interface AddFriendUpdateCallBack{
        void addFriendUpdated(ContactRecord contactRecord);
    }
}

