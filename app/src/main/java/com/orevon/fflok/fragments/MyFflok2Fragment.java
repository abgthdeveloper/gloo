package com.orevon.fflok.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.orevon.fflok.R;
import com.orevon.fflok.models.Place;
import com.orevon.fflok.utils.CommonUtils;
import com.orevon.fflok.utils.FflokDataSingleton;
import com.orevon.fflok.utils.internet.GPSTracker;
import com.orevon.fflok.utils.internet.NetworkUtils;
import com.orevon.fflok.utils.internet.VolleySingleton;
import com.orevon.fflok.views.ProgressHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.orevon.fflok.constants.ApiPaths.BASEURL;
import static com.orevon.fflok.constants.ApiPaths.VOTE_FFLOK;
import static com.orevon.fflok.constants.FflokConstants.X_API_KEY;

public class MyFflok2Fragment extends Fragment {
    BottomSheetBehavior bottomSheetBehavior;
    private static final int REQUEST_CODE_PERMISSION = 2;
    private String latitude, longitude;
    private CardView chat_bottom_sheet;
    private RelativeLayout topimageLay, bottomimageLay;
    private View toptint, bottomtint;
    boolean istemplateclicked = false;
    boolean isgiftemplateclicked = false;
    private ImageView closeButton, texttemplate, backButton;
    private RelativeLayout toplay;
    private ScrollView texttemplatelay;
    private RatingBar ratingBar1;
    private TextView placeName1, address1, distance1;
    RelativeLayout giftemplatelay;
    private ImageView giftemplate;
    private GPSTracker gps;
    private List<Place> proximitePlaces;
    private String selectedPlaceId;

    public MyFflok2Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_fflok2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        setupBottomSheet();
        clickListeners(view);
    }

    private void clickListeners(View view) {
        topimageLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toptint.setBackgroundColor(Color.parseColor("#AF50E3C2"));
                bottomtint.setBackgroundColor(Color.parseColor("#54000000"));
                callVoteFflokAPI();
            }
        });

        bottomimageLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toptint.setBackgroundColor(Color.parseColor("#54000000"));
                bottomtint.setBackgroundColor(Color.parseColor("#AF50E3C2"));
                callVoteFflokAPI();
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseDialogFragment fragment = CloseDialogFragment.newInstance();
                fragment.show(getActivity().getFragmentManager(), "");
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity()
                        .getSupportFragmentManager();
                fm.popBackStack ("HomeFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });

        toplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_HIDDEN) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });

        texttemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                giftemplate.setColorFilter(ContextCompat.getColor(getContext(), R.color.greycolor));
                giftemplatelay.setVisibility(View.GONE);
                isgiftemplateclicked = false;
                if (istemplateclicked) {
                    texttemplatelay.setVisibility(View.GONE);
                    istemplateclicked = false;
                    texttemplate.setColorFilter(ContextCompat.getColor(getContext(), R.color.greycolor));
                } else {
                    texttemplatelay.setVisibility(View.VISIBLE);
                    istemplateclicked = true;
                    texttemplate.setColorFilter(ContextCompat.getColor(getContext(), R.color.welcomeGreen));
                }

            }
        });

        giftemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                texttemplate.setColorFilter(ContextCompat.getColor(getContext(), R.color.greycolor));
                texttemplatelay.setVisibility(View.GONE);
                istemplateclicked = false;
                if (isgiftemplateclicked) {
                    giftemplatelay.setVisibility(View.GONE);
                    isgiftemplateclicked = false;
                    giftemplate.setColorFilter(ContextCompat.getColor(getContext(), R.color.greycolor));
                } else {
                    giftemplatelay.setVisibility(View.VISIBLE);
                    isgiftemplateclicked = true;
                    giftemplate.setColorFilter(ContextCompat.getColor(getContext(), R.color.welcomeGreen));
                }

            }
        });
    }

    private void setupBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(chat_bottom_sheet);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                } else if (newState == BottomSheetBehavior.STATE_COLLAPSED) {

                } else if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    private void initView(View view) {
        chat_bottom_sheet = view.findViewById(R.id.chat_bottom_sheet);
        topimageLay = view.findViewById(R.id.topimagelay);
        bottomimageLay = view.findViewById(R.id.bottomimagelay);
        toptint = view.findViewById(R.id.toptint);
        bottomtint = view.findViewById(R.id.bottomtint);
        closeButton = view.findViewById(R.id.closebutton);
        backButton = view.findViewById(R.id.backbutton);
        toplay = view.findViewById(R.id.toplay);
        texttemplate = view.findViewById(R.id.texttemplate);
        texttemplatelay = view.findViewById(R.id.texttemplatelay);
        giftemplatelay = view.findViewById(R.id.giftemplatelay);
        giftemplate = view.findViewById(R.id.giftemplate);
        ratingBar1 = view.findViewById(R.id.rating_bar);
        placeName1 = view.findViewById(R.id.place_name);
        address1 = view.findViewById(R.id.place_address);
        distance1 = view.findViewById(R.id.place_distance);
    }
//    private void setPlacesData() {
//        Place place = proximitePlaces.get(0);
//        Glide.with(this).load(place.getPathImg()).into(new SimpleTarget<Drawable>() {
//            @Override
//            public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//                    topimageLay.setBackground(resource);
//                }
//            }
//        });
//        ratingBar1.setRating(Float.valueOf(place.getVote()));
//        placeName1.setText(place.getName());
//        address1.setText(place.getAdresse());
//        Double distance = CommonUtils.getDistanceBetweenLocations(Double.valueOf(latitude), Double.valueOf(longitude), Double.valueOf(place.getLatitude()), Double.valueOf(place.getLongtitude()));
//        distance1.setText(String.format("%.1f", distance)+ " km");
//    }


    private void callVoteFflokAPI() {
        ProgressHandler progressHandler = new ProgressHandler(getContext());
        StringRequest request = voteFflokVolleyRequest(progressHandler);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getContext())
                .addToRequestQueue(request);
    }

    private StringRequest voteFflokVolleyRequest(final ProgressHandler progressHandler) {
        return new StringRequest(Request.Method.PUT, BASEURL + VOTE_FFLOK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        progressHandler.hide();
                        try {
                            JSONObject result = new JSONObject(response);
                            Boolean success = result.getBoolean("success");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressHandler.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getContext());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("idfflok", FflokDataSingleton.getInstance().getFflokId());
                params.put("idUser", FflokDataSingleton.getInstance().getUserId(getContext()));
                params.put("idPlace", selectedPlaceId);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getContext()));
                return headers;
            }
        };
    }




}

