package com.orevon.fflok.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.test.mock.MockPackageManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.orevon.fflok.R;
import com.orevon.fflok.adapters.MapInfoWindowAdapter;
import com.orevon.fflok.models.Place;
import com.orevon.fflok.utils.CommonUtils;
import com.orevon.fflok.utils.FflokDataSingleton;
import com.orevon.fflok.utils.internet.GPSTracker;
import com.orevon.fflok.utils.internet.NetworkUtils;
import com.orevon.fflok.utils.internet.VolleySingleton;
import com.orevon.fflok.views.ProgressHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.orevon.fflok.constants.ApiPaths.ADD_FAVOURITE_PLACE_EXPLORER;
import static com.orevon.fflok.constants.ApiPaths.BASEURL;
import static com.orevon.fflok.constants.ApiPaths.CHECK_FAVOURITE_PLACE_EXPLORER;
import static com.orevon.fflok.constants.ApiPaths.GET_PROXIMITEPLACES_EXPLORER;
import static com.orevon.fflok.constants.FflokConstants.X_API_KEY;

public class ExplorerFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;
    private MarkerOptions options = new MarkerOptions();

//    private LocationManager locationManager;

    //    MapView mMapView;
//    private GoogleMap googleMap;
    private static final int PERMISSIONS_REQUEST_LOCATION_EXPLORER = 4;
    List<Place> proximatePlacesExplorer;
    MapInfoWindowAdapter customInfoWindow;

    public ExplorerFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Window window = getActivity().getWindow();
            window.getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE);
            window.setStatusBarColor(ContextCompat.getColor(getContext(), android.R.color.transparent));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getActivity().getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_explorer, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.g_map);
//
//        if (mapFragment != null) {
//            locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
//            mapFragment.getMapAsync(this);
//        }
//        setMap(view);
        if (FflokDataSingleton.getInstance().getLatitude().equals("") || FflokDataSingleton.getInstance().getLongitude().equals("")) {
            getLocationGPSandMakeRequest();
        } else {
            callListProximitePlacesExplorerRequest();
        }
    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//
//        setupMap();
//        //seattle coordinates
////        LatLng seattle = new LatLng(47.6062095, -122.3320708);
////        mMap.addMarker(new MarkerOptions().position(seattle).title("Seattle"));
////        mMap.moveCamera(CameraUpdateFactory.newLatLng(seattle));
//    }
//
//    @Override
//    public void onLocationChanged(Location location) {
//        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
//        mMap.animateCamera(cameraUpdate);
//        locationManager.removeUpdates(this);
//    }
//
//    @Override
//    public void onStatusChanged(String provider, int status, Bundle extras) {
//
//    }
//
//    @Override
//    public void onProviderEnabled(String provider) {
//
//    }
//
//    @Override
//    public void onProviderDisabled(String provider) {
//
//    }
//
//    private void setupMap() {
//
//        if (ActivityCompat.checkSelfPermission(getContext(),
//                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
//                && ActivityCompat.checkSelfPermission(getContext(),
//                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        mMap.setMyLocationEnabled(true);
//
//        // Get LocationManager object from System Service LOCATION_SERVICE
//        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
//
//        // Create a criteria object to retrieve provider
//        Criteria criteria = new Criteria();
//
//        // Get the name of the best provider
//        String provider = locationManager.getBestProvider(criteria, true);
//
//        // Get Current Location
//        Location myLocation = locationManager.getLastKnownLocation(provider);
//
//        //set map type
//        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
//
//        // Get latitude of the current location
//        double latitude = myLocation.getLatitude();
//
//        // Get longitude of the current location
//        double longitude = myLocation.getLongitude();
//
//        // Create a LatLng object for the current location
//        LatLng latLng = new LatLng(latitude, longitude);
//
//        // Show the current location in Google Map
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//
//        // Zoom in the Google Map
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(20));
//        mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("You are here!"));
//    }

    private void setMap() {
        Activity activity = getActivity();
        if(activity != null && isAdded()) {//If only attached to fragment
            SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map_view);
            mapFragment.getMapAsync(this);
        }
    }

    private void callListProximitePlacesExplorerRequest() {
        ProgressHandler progressHandler = new ProgressHandler(getContext());
        StringRequest request = getProximatePlacesExplorerVolleyRequest(progressHandler);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void checkFavPlacesExplorerRequest(String placeId, Marker marker) {
        ProgressHandler progressHandler = new ProgressHandler(getContext());
        StringRequest request = checkFavPlacesExplorerVolleyRequest(progressHandler, placeId, marker);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void addFavPlacesExplorerRequest(String placeId, Marker marker) {
        ProgressHandler progressHandler = new ProgressHandler(getContext());
        StringRequest request = addFavPlaceExplorerVolleyRequest(progressHandler, placeId, marker);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private StringRequest getProximatePlacesExplorerVolleyRequest(final ProgressHandler progressHandler) {
        return new StringRequest(Request.Method.POST, BASEURL + GET_PROXIMITEPLACES_EXPLORER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        progressHandler.hide();
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonarray = jsonResponse.getJSONArray("places ");
                            proximatePlacesExplorer = new ArrayList<>();
                            for (int i = 0; i < jsonarray.length(); i++) {
                                Gson gson = new Gson();
                                Place place = gson.fromJson(jsonarray.get(i).toString(), Place.class);
                                proximatePlacesExplorer.add(place);
                            }
                            setMap();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getContext());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("latitude", String.valueOf(FflokDataSingleton.getInstance().getLatitude()));
                params.put("longitude", String.valueOf(FflokDataSingleton.getInstance().getLongitude()));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getContext()));
                return headers;
            }
        };
    }

    private StringRequest checkFavPlacesExplorerVolleyRequest(final ProgressHandler progressHandler, final String idPlace, final Marker marker) {
        return new StringRequest(Request.Method.POST, BASEURL + CHECK_FAVOURITE_PLACE_EXPLORER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        progressHandler.hide();
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            Boolean favourite = jsonResponse.getBoolean("favorite");
                            Place place = (Place) marker.getTag();
                            place.setSelected(favourite);
                            marker.setTag(place);
                            customInfoWindow = new MapInfoWindowAdapter(getContext());
                            mMap.setInfoWindowAdapter(customInfoWindow);
                            marker.showInfoWindow();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getContext());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("idPlace", idPlace);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getContext()));
                return headers;
            }
        };
    }

    private StringRequest addFavPlaceExplorerVolleyRequest(final ProgressHandler progressHandler, final String idPlace, final Marker marker) {
        return new StringRequest(Request.Method.PUT, BASEURL + ADD_FAVOURITE_PLACE_EXPLORER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        progressHandler.hide();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getContext());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("idPlace", idPlace);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getContext()));
                return headers;
            }
        };
    }

    private void getLocationGPSandMakeRequest() {
        try {
            if(ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)   {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSIONS_REQUEST_LOCATION_EXPLORER);
                // If any permission above not allowed by user, this condition will execute every time, else your else part will work
            } else {
                GPSTracker gps = new GPSTracker(getContext());
                // check if GPS enabled
                if (gps.canGetLocation()) {

//                    String latitude = String.valueOf(gps.getLatitude());
//                    String longitude = String.valueOf(gps.getLongitude());
                    FflokDataSingleton.getInstance().setLatitude(gps.getLatitude());
                    FflokDataSingleton.getInstance().setLongitude(gps.getLongitude());
                    callListProximitePlacesExplorerRequest();
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_LOCATION_EXPLORER: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocationGPSandMakeRequest();
                } else {
                    // Permission was not granted.
                    Toast.makeText(this.getContext(), R.string.permission_denied_access_location, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            mMap = googleMap;

            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSIONS_REQUEST_LOCATION_EXPLORER);
            }
            googleMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setZoomGesturesEnabled(true);
            mMap.setMinZoomPreference(15);

            LatLng latLng = null;
            for (int i = 0; i < proximatePlacesExplorer.size(); i++) {
                Place place = proximatePlacesExplorer.get(i);
                if(place.getLatitude() == null || place.getLatitude().equals("") || place.getLongtitude() == null || place.getLongtitude().equals("") ) {
                    //do nothing
                } else {
                    latLng = new LatLng(Double.valueOf(place.getLatitude()), Double.valueOf(place.getLongtitude()));
                    options.position(latLng);
                    options.title(place.getName());
                    Marker marker = googleMap.addMarker(options);
                    marker.setTag(place);
                    marker.showInfoWindow();
                    marker.hideInfoWindow();
                }
            }
            if(latLng == null) {
                latLng = new LatLng(Double.valueOf(FflokDataSingleton.getInstance().getLatitude()), Double.valueOf(FflokDataSingleton.getInstance().getLongitude()));
            }
            CameraUpdate cu = CameraUpdateFactory.newLatLng(latLng);
            mMap.moveCamera(cu);
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            mMap.setOnInfoWindowClickListener(this);
            mMap.setOnMarkerClickListener(this);
        }catch (Exception e) {
            Toast.makeText(getContext(), R.string.error_loading_map , Toast.LENGTH_SHORT).show();
            Log.e("EXCEPTION MAP LOADING", "exception", e);
        }

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        addFavPlacesExplorerRequest(((Place)marker.getTag()).getId(), marker);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        checkFavPlacesExplorerRequest(((Place)marker.getTag()).getId(), marker);
        return false;
    }
}
