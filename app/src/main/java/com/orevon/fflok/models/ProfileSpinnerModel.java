package com.orevon.fflok.models;

public class ProfileSpinnerModel {
    String title;
    int imageId;
    int selectedImageId;

    public ProfileSpinnerModel(String title, int imageId, int selectedImageId) {
        this.title = title;
        this.imageId = imageId;
        this.selectedImageId = selectedImageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public int getSelectedImageId() {
        return selectedImageId;
    }

    public void setSelectedImageId(int selectedImageId) {
        this.selectedImageId = selectedImageId;
    }
}
