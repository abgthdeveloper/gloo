package com.orevon.fflok.models;

import com.google.gson.annotations.SerializedName;

public class UserContactFeature {
    private Boolean fflok = false;
    private String badge;
    private Boolean friend = false;
    @SerializedName("path_img")
    private String pathImage;
    private String phone;

    public Boolean getFflok() {
        return fflok;
    }

    public void setFflok(Boolean fflok) {
        this.fflok = fflok;
    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }

    public Boolean getFriend() {
        return friend;
    }

    public void setFriend(Boolean friend) {
        this.friend = friend;
    }

    public String getPathImage() {
        return pathImage;
    }

    public void setPathImage(String pathImage) {
        this.pathImage = pathImage;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
