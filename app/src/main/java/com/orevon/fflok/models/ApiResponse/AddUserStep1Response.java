package com.orevon.fflok.models.ApiResponse;

import com.google.gson.annotations.SerializedName;

public class AddUserStep1Response {

    @SerializedName("new")
    private Boolean newUser;
    private String token;

    public Boolean getNewUser() {
        return newUser;
    }

    public void setNewUser(Boolean newUser) {
        this.newUser = newUser;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
