package com.orevon.fflok.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProximateFriend {
    private Boolean friend;
    private Integer score;
    private String phone;
    private String id;
    @SerializedName("path_img")
    private String pathImg;

    public Boolean getFriend() {
        return friend;
    }

    public void setFriend(Boolean friend) {
        this.friend = friend;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPathImg() {
        return pathImg;
    }

    public void setPathImg(String pathImg) {
        this.pathImg = pathImg;
    }
}
