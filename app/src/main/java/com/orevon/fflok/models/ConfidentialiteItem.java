package com.orevon.fflok.models;

import com.orevon.fflok.enums.ConfidentialiteItemType;

import java.util.List;

public class ConfidentialiteItem {
    String title;
    ConfidentialiteItemType itemType = ConfidentialiteItemType.TimeLine;
    boolean isExpanded = false;
    int levelCompleted = 0;
    List<SwitchItem> switchItems;
    List<DataPartner> dataPartners;

    public ConfidentialiteItem(String title, ConfidentialiteItemType itemType, boolean isExpanded) {
        this.title = title;
        this.itemType = itemType;
        this.isExpanded = isExpanded;

    }

    public ConfidentialiteItemType getItemType() {
        return itemType;
    }

    public void setItemType(ConfidentialiteItemType itemType) {
        this.itemType = itemType;
    }



    public boolean isExpanded() {

        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public List<SwitchItem> getSwitchItems() {
        return switchItems;
    }

    public void setSwitchItems(List<SwitchItem> switchItems) {
        this.switchItems = switchItems;

    }

    public List<DataPartner> getDataPartners() {
        return dataPartners;
    }

    public void setDataPartners(List<DataPartner> dataPartners) {
        this.dataPartners = dataPartners;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLevelCompleted() {
        return levelCompleted;
    }

    public void setLevelCompleted(int levelCompleted) {
        this.levelCompleted = levelCompleted;
    }

}