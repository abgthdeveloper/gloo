package com.orevon.fflok.models.ApiResponse;

import com.google.gson.annotations.SerializedName;

public class AddUserFriendResponse {
    private Boolean success;
    @SerializedName("user_fflok")
    private Boolean userFflok;
    private Boolean invitation;
    @SerializedName("err")
    private String error;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Boolean getUserFflok() {
        return userFflok;
    }

    public void setUserFflok(Boolean userFflok) {
        this.userFflok = userFflok;
    }

    public Boolean getInvitation() {
        return invitation;
    }

    public void setInvitation(Boolean invitation) {
        this.invitation = invitation;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}