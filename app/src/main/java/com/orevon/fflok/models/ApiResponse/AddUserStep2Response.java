package com.orevon.fflok.models.ApiResponse;

import com.orevon.fflok.models.User;

public class AddUserStep2Response {
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
