package com.orevon.fflok.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponse {

    @SerializedName("error_code")
    private String errorCode;

    private float cost;

    private float balance;

    private String ticket;

    @SerializedName("sending_date")
    private long sendingDate;

    @SerializedName("number_of_sendings")
    private int numberOfSendings;

    @SerializedName("currency_code")
    private String currencyCode;

    private List<LoginResponse> successs;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public long getSendingDate() {
        return sendingDate;
    }

    public void setSendingDate(long sendingDate) {
        this.sendingDate = sendingDate;
    }

    public int getNumberOfSendings() {
        return numberOfSendings;
    }

    public void setNumberOfSendings(int numberOfSendings) {
        this.numberOfSendings = numberOfSendings;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public List<LoginResponse> getSuccesss() {
        return successs;
    }

    public void setSuccesss(List<LoginResponse> successs) {
        this.successs = successs;
    }
}
