package com.orevon.fflok.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class User {
    private Username username;
    private Boolean active;
    private Integer démocrate;
    @SerializedName("number_fflok")
    private Integer numberFflok;
    private Object statu;
    private String lang;
    @SerializedName("_id")
    private String id;
    private String phone;
    private String code;
    @SerializedName("path_img")
    private String pathImage;
    @SerializedName("path_video")
    private String pathVideo;
    private List<Object> adresse = null;
    private List<Object> age = null;
    private List<Object> email = null;
    private List<Object> fflok = null;
    private List<Liker> likers = null;
    @SerializedName("favorite_place")
    private List<Object> favoritePlace = null;
    private List<Badge> badge = null;
    @SerializedName("create_date")
    private String createDate;
    @SerializedName("__v")
    private Integer v;
    private List<Float> geo = null;
    private String latitude;
    private String longitude;
    private String registrationToken;

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getDémocrate() {
        return démocrate;
    }

    public void setDémocrate(Integer démocrate) {
        this.démocrate = démocrate;
    }

    public Integer getNumberFflok() {
        return numberFflok;
    }

    public void setNumberFflok(Integer numberFflok) {
        this.numberFflok = numberFflok;
    }

    public Object getStatu() {
        return statu;
    }

    public void setStatu(Object statu) {
        this.statu = statu;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPathImage() {
        return pathImage;
    }

    public void setPathImage(String pathImage) {
        this.pathImage = pathImage;
    }

    public String getPathVideo() {
        return pathVideo;
    }

    public void setPathVideo(String pathVideo) {
        this.pathVideo = pathVideo;
    }

    public List<Object> getAdresse() {
        return adresse;
    }

    public void setAdresse(List<Object> adresse) {
        this.adresse = adresse;
    }

    public List<Object> getAge() {
        return age;
    }

    public void setAge(List<Object> age) {
        this.age = age;
    }

    public List<Object> getEmail() {
        return email;
    }

    public void setEmail(List<Object> email) {
        this.email = email;
    }

    public List<Object> getFflok() {
        return fflok;
    }

    public void setFflok(List<Object> fflok) {
        this.fflok = fflok;
    }

    public List<Liker> getLikers() {
        return likers;
    }

    public void setLikers(List<Liker> likers) {
        this.likers = likers;
    }

    public List<Object> getFavoritePlace() {
        return favoritePlace;
    }

    public void setFavoritePlace(List<Object> favoritePlace) {
        this.favoritePlace = favoritePlace;
    }

    public List<Badge> getBadge() {
        return badge;
    }

    public void setBadge(List<Badge> badge) {
        this.badge = badge;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public List<Float> getGeo() {
        return geo;
    }

    public void setGeo(List<Float> geo) {
        this.geo = geo;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getRegistrationToken() {
        return registrationToken;
    }

    public void setRegistrationToken(String registrationToken) {
        this.registrationToken = registrationToken;
    }

    public Username getUsername() {
        return username;
    }

    public void setUsername(Username username) {
        this.username = username;
    }
}
