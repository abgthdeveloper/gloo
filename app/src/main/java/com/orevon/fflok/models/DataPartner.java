package com.orevon.fflok.models;

import java.util.List;

public class DataPartner {
    int partnerImageId;
    List<DataShared> dataSharedList;

    public DataPartner(int partnerImageId, List<DataShared> dataSharedList) {
        this.partnerImageId = partnerImageId;
        this.dataSharedList = dataSharedList;
    }

    public int getPartnerImageId() {
        return partnerImageId;
    }

    public void setPartnerImageId(int partnerImageId) {
        this.partnerImageId = partnerImageId;
    }

    public List<DataShared> getDataSharedList() {
        return dataSharedList;
    }

    public void setDataSharedList(List<DataShared> dataSharedList) {
        this.dataSharedList = dataSharedList;
    }
}
