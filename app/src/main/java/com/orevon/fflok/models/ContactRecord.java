package com.orevon.fflok.models;

/**
 * Created by priji on 16/1/18.
 */


import android.graphics.Bitmap;

/**
 * A record from the system contact db
 */
public class ContactRecord {

    private String name;
    private String initials;
    private String phone;
    private Bitmap bitmapThumbnail;
    private String email;
    private String source;
    private String badgePath;

    private boolean isBadgePresent = false;
    private boolean isSuperFflocker;
    private boolean isFflockerFriend;

    public String getBadgePath() {
        return badgePath;
    }

    public void setBadgePath(String badgePath) {
        this.badgePath = badgePath;
    }


    public boolean isBadgePresent() {
        return isBadgePresent;
    }

    public void setBadgePresent(boolean badgePresent) {
        isBadgePresent = badgePresent;
    }

    ////////////////////////////////////
    // Name
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    ////////////////////////////////////
    // Initials
    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    ////////////////////////////////////
    // Phone
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    ////////////////////////////////////
    // Thumbnail
    public Bitmap getThumbnail() {
        return bitmapThumbnail;
    }

    public void setThumb(Bitmap thumbnail) {
        this.bitmapThumbnail = thumbnail;
    }

    ////////////////////////////////////
    // Is Super Fflocker?
    public boolean isSuperFflocker() {
        return isSuperFflocker;
    }

    public void setSuperFflocker(boolean isSuperFflocker) {
        this.isSuperFflocker = isSuperFflocker;
    }

    ////////////////////////////////////
    // Is Fflocker?
    public boolean isFflockerFriend() {
        return isFflockerFriend;
    }

    public void setFflockerFriend(boolean isFflocker) {
        this.isFflockerFriend = isFflocker;
    }
}
