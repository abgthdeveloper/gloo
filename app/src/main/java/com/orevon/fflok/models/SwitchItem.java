package com.orevon.fflok.models;

public class SwitchItem {
        String title;
        boolean isChecked;
        int imageId = 0;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public SwitchItem(String title, boolean isChecked, int imageId) {
            this.title = title;
            this.isChecked = isChecked;
            this.imageId = imageId;

        }

    public SwitchItem(String title, boolean isChecked) {
        this.title = title;
        this.isChecked = isChecked;
    }
}
