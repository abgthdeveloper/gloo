package com.orevon.fflok.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.github.vivchar.viewpagerindicator.ViewPagerIndicator;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.orevon.fflok.R;
import com.orevon.fflok.models.Place;
import com.orevon.fflok.models.Title;
import com.orevon.fflok.utils.CommonUtils;
import com.orevon.fflok.utils.FflokDataSingleton;

import java.util.ArrayList;
import java.util.List;

public class PlaceListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    private List<Place> list;
    private Context context;
    private PlaceClickedCallBack callback;


    public PlaceListAdapter(Context context, List<Place> data) {
        this.context = context;
        list = data;
    }

    class PlaceViewHolder extends RecyclerView.ViewHolder {

        public TextView placeImageDescription;
        public ImageView placeImageView;
        public SimpleRatingBar placeRatingBar;
        public TextView placeNameTextView;
        public TextView placeAddress1TextView;
        public TextView placeDistanceTextView;
        public View tintView;

        public PlaceViewHolder(View v) {
            super(v);
            placeImageDescription = (TextView) v.findViewById(R.id.place_image_description);
            placeImageView = (ImageView) v.findViewById(R.id.spot1);
            placeRatingBar = v.findViewById(R.id.spot1Rating);
            placeNameTextView = (TextView) v.findViewById(R.id.spot1Name);
            placeAddress1TextView = (TextView) v.findViewById(R.id.spot1Address1);
            placeDistanceTextView = (TextView) v.findViewById(R.id.spot1Distance);
            tintView = (View) v.findViewById(R.id.toptint);
        }

    }

    class PlacePagerViewHolder extends RecyclerView.ViewHolder {
        public ViewPager pager;
        public ViewPagerIndicator pagerIndicator;
        public SimpleRatingBar pagerRatingBar;
        public TextView pagerNameTextView;
        public TextView pagerAddressTextView;
        public TextView pagerDistanceTextView;

//        public ImageView placeImageView;

        public PlacePagerViewHolder(View v) {
            super(v);
            pager = (ViewPager) v.findViewById(R.id.viewpager);
            pagerIndicator = (ViewPagerIndicator) v.findViewById(R.id.view_pager_indicator);
            pagerRatingBar = (SimpleRatingBar) v.findViewById(R.id.spot2Rating);
            pagerNameTextView = (TextView) v.findViewById(R.id.spot2Name);

//            placeImageView = (ImageView) v.findViewById(R.id.spot1);
            pagerAddressTextView = (TextView) v.findViewById(R.id.spot2Address1);
            pagerDistanceTextView = (TextView) v.findViewById(R.id.spot2Distance);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(list.get(position).getPathImg() != "") { //TODO:check condition as pathimagecount-consult with them
            return 0;
        } else return 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        switch (viewType) {
            case 0:
                View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.proximate_place_item, parent, false);
                PlaceViewHolder holder = new PlaceViewHolder(view);
                return holder;
            case 1:
                View view1 = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.proximate_place_view_pager_item, parent, false);
                PlacePagerViewHolder holder1 = new PlacePagerViewHolder(view1);
                return holder1;

        }
        return null;

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case 0:
                final Place place = list.get(position);
                final PlaceViewHolder holder1 = (PlaceViewHolder) holder;
                Glide.with(context).load(place.getPathImg()).into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            holder1.placeImageView.setImageDrawable(resource);
//                    topimageLay.setBackground(resource);
                        }
                    }
                });
                holder1.placeNameTextView.setText(place.getName());
                holder1.placeAddress1TextView.setText(place.getAdresse());
                holder1.placeRatingBar.setRating(Float.valueOf(place.getVote()));
                Double distance = CommonUtils.getDistanceBetweenLocations(FflokDataSingleton.getInstance().getLatitude(), FflokDataSingleton.getInstance().getLongitude(), Double.valueOf(place.getLatitude()), Double.valueOf(place.getLongtitude()));
                holder1.placeDistanceTextView.setText("a " + String.format("%.1f",distance) + " km");
                holder1.placeImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(callback != null) {
                            callback.placeClicked(position, place.getId());
                        }
                    }
                });
                if(place.getSelected()) {
                    holder1.tintView.setBackgroundColor(Color.parseColor("#AF50E3C2"));
                } else {
                    holder1.tintView.setBackgroundColor(Color.TRANSPARENT);
                }
                break;

            case 2:
                //TODO:fill items for case 2
                break;
        }
    }

//    @Override
//    public void onBindViewHolder(final PlaceViewHolder holder, final int position) {
//        final Place place = list.get(position);
//        Glide.with(context).load(place.getPathImg()).into(new SimpleTarget<Drawable>() {
//            @Override
//            public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//                    holder.placeImageView.setImageDrawable(resource);
////                    topimageLay.setBackground(resource);
//                }
//            }
//        });
//        holder.placeNameTextView.setText(place.getName());
//        holder.placeAddress1TextView.setText(place.getAdresse());
//        holder.placeRatingBar.setRating(Float.valueOf(place.getVote()));
//        Double distance = CommonUtils.getDistanceBetweenLocations(FflokDataSingleton.getInstance().getLatitude(), FflokDataSingleton.getInstance().getLongitude(), Double.valueOf(place.getLatitude()), Double.valueOf(place.getLongtitude()));
//        holder.placeDistanceTextView.setText("a " + String.valueOf(distance) + "km");
//        holder.placeImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(callback != null) {
//                    callback.placeClicked(position, place.getId());
//                }
//            }
//        });
//    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setCallback(PlaceClickedCallBack callback){
        this.callback = callback;
    }

    public interface PlaceClickedCallBack{
        public void placeClicked(int position, String placeId);
    }
}
