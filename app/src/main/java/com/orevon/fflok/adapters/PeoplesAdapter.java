package com.orevon.fflok.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.orevon.fflok.R;

public class PeoplesAdapter extends RecyclerView.Adapter<PeoplesAdapter.ViewHolder> {

    //        private final List<Item> mItems;
    Context mContext;

    public PeoplesAdapter(Context context) {
//            mItems = items;
        mContext = context;
    }

    @Override
    public PeoplesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fflok_people, parent, false);
        return new PeoplesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PeoplesAdapter.ViewHolder holder, int position) {
//            holder.mItem = mItems.get(position);
//            holder.title.setText(holder.mItem.getName());
//            Picasso.with(mContext).load(holder.mItem.getImage()).into(holder.image);

    }

    @Override
    public int getItemCount() {
//            return mItems.size();
        return 3;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
//            public Item mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
        }

        @Override
        public String toString() {
            return super.toString() + " '" + "" + "'";
        }
    }
}
