package com.orevon.fflok.adapters;

/**
 * Created by priji on 16/1/18.
 */


import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.orevon.fflok.R;
import com.orevon.fflok.models.ContactRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ContactRecordAdapter extends BaseAdapter {
    private List<ContactRecord> contactRecordList;
    private Context context;
    private AddFriendClickedCallBack callback;
    private ArrayList<ContactRecord> arraylist;

    public List<ContactRecord> getContactRecordList() {
        return contactRecordList;
    }

    public void setContactRecordList(List<ContactRecord> contactRecordList) {
        this.contactRecordList = contactRecordList;
        this.arraylist.clear();
        this.arraylist.addAll(contactRecordList);

    }

    public ContactRecordAdapter(List<ContactRecord> contactRecords, Context context) {
        contactRecordList = contactRecords;
        this.context = context;
        this.arraylist = new ArrayList<ContactRecord>();
        this.arraylist.addAll(contactRecords);
    }



    @Override
    public int getCount() {
        return contactRecordList.size();
    }

    @Override
    public Object getItem(int i) {
        return contactRecordList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view == null) {
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.fragment_contact, null);
        } else {
            view = convertView;
        }
        view.setClickable(true);

        TextView textViewContactName = (TextView) view.findViewById(R.id.textViewContactName);
        TextView textViewContactPhoneNumber = (TextView) view.findViewById(R.id.textViewContactPhoneNumber);

        final ContactRecord data = (ContactRecord) contactRecordList.get(i);

        textViewContactName.setText(data.getName());
        textViewContactPhoneNumber.setText(data.getPhone());

        View imageViewSuperFflocker = view.findViewById(R.id.imageViewSuperFflocker);
//        if (data.isSuperFflocker()) {
//            imageViewSuperFflocker.setVisibility(View.VISIBLE);
//        } else {
            imageViewSuperFflocker.setVisibility(View.GONE);
//        }

        ImageView badgeIcon = (ImageView) view.findViewById(R.id.badgeImageView) ;
        Button imageViewFflocker = (Button)view.findViewById(R.id.imageViewFflocker);
        imageViewFflocker.setClickable(true);
        if (data.isFflockerFriend()) {
            imageViewFflocker.setBackground(context.getDrawable(R.drawable.heart_filled1));
        } else {
            imageViewFflocker.setBackground(context.getDrawable(R.drawable.heart_unfilled1));
        }

        if(data.isBadgePresent()) {
            badgeIcon.setVisibility(View.VISIBLE);
            Glide.with(context)
                .load(data.getBadgePath()) // Uri of the picture
                .apply(RequestOptions.circleCropTransform())
                .into(badgeIcon);
        } else {
            badgeIcon.setVisibility(View.GONE);
        }
        final int position = i;
        imageViewFflocker.setOnClickListener(new View.OnClickListener() {
            @Override
             public void onClick(View v) {
                if(callback != null) {
                    callback.addFriendClicked(position, data);
                }
            }
        });

//        buttonInviteContact.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    buttonInviteContact.setLayoutParams(new LinearLayout.LayoutParams(
//                            AndroidUtils.dpToPx(context, 16), AndroidUtils.dpToPx(context, 16)));
//                    buttonInviteContact.setEnabled(false);
//                } else {
//                    buttonInviteContact.setLayoutParams(new LinearLayout.LayoutParams(
//                            AndroidUtils.dpToPx(context, 64), AndroidUtils.dpToPx(context, 24)));
//                }
//            }
//        });


        // Set image if exists
        ImageView imageViewProfilePic = (ImageView) view.findViewById(R.id.imageViewProfilePic);
        imageViewProfilePic.setImageDrawable(null);
        TextView textViewInitials = (TextView) view.findViewById(R.id.textViewInitials);
        textViewInitials.setText(null);

        try {

            if (data.getThumbnail() != null) {
                imageViewProfilePic.setImageDrawable(null);

                Bitmap bmp = data.getThumbnail();
                RoundedBitmapDrawable roundedBmpDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), bmp);
                roundedBmpDrawable.setCircular(true);
                // imageViewProfilePic.setImageBitmap(data.getThumbnail());

                imageViewProfilePic.setVisibility(View.VISIBLE);
                textViewInitials.setVisibility(View.GONE);
                imageViewProfilePic.setImageDrawable(roundedBmpDrawable);
            } else {
                if (data.getInitials() != null) {
                    textViewInitials.setText(data.getInitials());
                    imageViewProfilePic.setVisibility(View.GONE);
                    textViewInitials.setVisibility(View.VISIBLE);
                }
            }
            // Seting round image
            // Bitmap bm = BitmapFactory.decodeResource(view.getResources(), R.drawable.round); // Load default image
            // roundedImage = new RoundImage(bm);
            // v.imageView.setImageDrawable(new BitmapDrawable(_c.getResources(), bm));
        } catch (OutOfMemoryError e) {
            // Add default picture
            // v.imageView.setImageDrawable(this._c.getDrawable(R.drawable.image));
            e.printStackTrace();
        }

        view.setTag(data);
        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        contactRecordList.clear();
        if (charText.length() == 0) {
            contactRecordList.addAll(arraylist);
        } else {
            for (ContactRecord record : arraylist) {
                if (record.getName().toLowerCase(Locale.getDefault()).startsWith(charText)) {
                    contactRecordList.add(record);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void setCallback(AddFriendClickedCallBack callback){
        this.callback = callback;
    }

    public interface AddFriendClickedCallBack{
        public void addFriendClicked(int position, ContactRecord contactRecord);
    }
}