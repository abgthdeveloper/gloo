package com.orevon.fflok.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.orevon.fflok.R;
import com.orevon.fflok.models.ConfidentialiteItem;
import com.orevon.fflok.models.SwitchItem;
import com.orevon.fflok.views.ConfidentialitePartnerView;
import com.orevon.fflok.views.ConfidentialiteSwitchItem;
import com.orevon.fflok.views.ConfidentialiteTonAttitudeView;

import java.util.List;

public class ConfidentialiteListAdapter extends RecyclerView.Adapter<ConfidentialiteListAdapter.MyViewHolder> {

    private List<ConfidentialiteItem> itemList;
    private Context context;
    private OnItemClicked onClick;
    public interface OnItemClicked {
        void onItemClick(int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public RelativeLayout dropdownView;
        public CardView cardView;
        public LinearLayout expandableView;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.titleTextView);
            cardView = (CardView)view.findViewById(R.id.rateCardView);
            dropdownView = (RelativeLayout) view.findViewById(R.id.dropdownView);
            expandableView = view.findViewById(R.id.expandableView);
        }
    }


    public ConfidentialiteListAdapter(List<ConfidentialiteItem> itemList, Context context) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.confidentialite_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ConfidentialiteItem item = itemList.get(position);
        holder.title.setText(item.getTitle());
        holder.dropdownView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.onItemClick(position);
            }
        });

        if (item.isExpanded()) {
            holder.expandableView.setVisibility(View.VISIBLE);
            holder.expandableView.removeAllViews();
            switch (item.getItemType()) {
                case TimeLine:
                    ConfidentialiteTonAttitudeView itemView = new ConfidentialiteTonAttitudeView(holder.itemView.getContext());
                    itemView.setLevelCompleted(1);
                    holder.expandableView.addView(itemView);
                    break;
                case CheckBoxList:
                    LinearLayout leftChild = new LinearLayout(context);
                    leftChild.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1));
                    leftChild.setOrientation(LinearLayout.VERTICAL);
                    leftChild.removeAllViews();

                    LinearLayout rightChild = new LinearLayout(context);
                    rightChild.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1));
                    rightChild.setOrientation(LinearLayout.VERTICAL);
                    rightChild.removeAllViews();

                    if (item.getSwitchItems() != null) {
                        for (SwitchItem switchItem: item.getSwitchItems()) {
                            ConfidentialiteSwitchItem view = new ConfidentialiteSwitchItem(holder.itemView.getContext());
                            view.switchText.setText(switchItem.getTitle());
                            view.aSwitch.setChecked(switchItem.isChecked());
                            if (switchItem.getImageId() != 0) {
                                view.switchItemImage.setImageResource(switchItem.getImageId());
                                view.switchItemImage.setVisibility(View.VISIBLE);
                            } else {
                                view.switchItemImage.setVisibility(View.GONE);
                            }

                            if (leftChild.getChildCount() > 3) {
                                rightChild.addView(view);
                            } else {
                                leftChild.addView(view);
                            }
//                            holder.expandableView.addView(view);
                        }
                        holder.expandableView.addView(leftChild);
                        holder.expandableView.addView(rightChild);

                    }
                    break;
                case PartnersList:
                    ConfidentialitePartnerView partnerView = new ConfidentialitePartnerView(holder.itemView.getContext());
                    partnerView.setData(item.getDataPartners());
                    holder.expandableView.addView(partnerView);
                    break;
            }
        } else {
            holder.expandableView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void setOnClick(OnItemClicked onClick) {
        this.onClick = onClick;
    }
}
