package com.orevon.fflok.utils.internet;

import android.content.Context;

import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;

import okhttp3.OkHttpClient;

/**
 * Created by kzhu9 on 12/4/15.
 */
public class OkHttpSingleton {
    private static OkHttpSingleton instance;
    private static OkHttpClient client;

    private OkHttpSingleton() {
    }

    public static synchronized OkHttpSingleton getInstance() {
        if (instance == null) {
            instance = new OkHttpSingleton();
        }
        return instance;
    }

    public synchronized OkHttpClient getClient(Context context) {
        if (client == null) {
            ClearableCookieJar cookieJar =
                    new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
            return new OkHttpClient.Builder()
                    .cookieJar(cookieJar)
                    .build();
//            client.read
//            client.writeTimeoutMillis(1800000);
//            client.readTimeoutMillis(1800000);
//            client.setCookieHandler(new CookieManager(
//                    new PersistentCookieStore(context),
//                    CookiePolicy.ACCEPT_ALL));
        }
        return client;
    }
}