package com.orevon.fflok.enums;

public enum ConfidentialiteItemType {
    TimeLine, CheckBoxList, PartnersList
}
