package com.orevon.fflok.constants;

public class ApiPaths {

    public static String LOGIN = "https://www.octopush-dm.com/api/sms/json";
    public static String BASEURL = "https://dev.fflok.orevonlabs.fr/api";
    public static String BASE_VIDEO_URL = "https://dev.fflok.orevonlabs.fr/public/users/";
    public static String ADD_USER_STEP1 = "/users/step1/addUser";
    public static String ADD_USER_STEP2 = "/users/step2/addUser";
    public static String ADD_USER_STEP3 = "/users/step3/addUser/";
    public static String ADD_USER_STEP4 ="/users/step4/addUser/";
    public static String ADD_FRIEND = "/users/add-friend";
    public static String CHECK_USERS = "/users/checkUsers";
    public static String GET_USER_FRIENDS = "/users/GetFriends";
    public static String GET_USER_PROFILE = "/users/getMyProfil";
    public static String GET_PROXIMITE_FRIENDS = "/users/getProximiteFriends";
    public static String GET_PROXIMITE_PLACE = "/users/getProximitePlace";
    public static String GET_TAGS_FFLOK = "/users/getTagsfflok";
    public static String FIND_TAGS_FFLOK = "/users/findTagsfflok";
    public static String CREATE_FFLOK = "/users/createfflok";
    public static String UPDATE_PLACES_FFLOK = "/users/updatePlacefflok";
    public static String ACCEPT_FFLOK = "/users/acceptFFlok";
    public static String VOTE_FFLOK = "/users/voteFFlok";
    public static String GET_FFLOK_DETAILS = "/users/getfflok/";
    public static String GET_RANDOM_USER = "/users/getRandomUser";
    public static String MATCH_FRIEND = "/users/matchFriend";
    public static String GET_PROXIMITEPLACES_EXPLORER = "/users/getProximitePlaceExplorer";
    public static String CHECK_FAVOURITE_PLACE_EXPLORER = "/users/checkFavoritePlace";
    public static String ADD_FAVOURITE_PLACE_EXPLORER = "/users/addFavoritePlace";
}
