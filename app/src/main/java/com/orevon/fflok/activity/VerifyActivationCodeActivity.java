package com.orevon.fflok.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.test.mock.MockPackageManager;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.orevon.fflok.R;
import com.orevon.fflok.models.ApiResponse.AddUserStep2Response;
import com.orevon.fflok.utils.CommonUtils;
import com.orevon.fflok.utils.FflokDataSingleton;
import com.orevon.fflok.utils.internet.GPSTracker;
import com.orevon.fflok.utils.internet.NetworkUtils;
import com.orevon.fflok.utils.internet.VolleySingleton;
import com.orevon.fflok.views.ProgressHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.orevon.fflok.constants.FflokConstants.X_API_KEY;
import static com.orevon.fflok.constants.ApiPaths.ADD_USER_STEP1;
import static com.orevon.fflok.constants.ApiPaths.ADD_USER_STEP2;
import static com.orevon.fflok.constants.ApiPaths.BASEURL;

public class VerifyActivationCodeActivity extends AppCompatActivity {

    public static String PHONE_NUMBER = "phoneNumber";

    private String latitude, longitude;
    private String phoneNumber;
    private static final int PERMISSIONS_REQUEST_LOCATION = 2;

    LocationManager locationManager;
    private PinEntryEditText pinEntry;
    GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_activation_code);
        initBottomImageSlideInAnimation();
        initOtpEditText();
        phoneNumber = getIntent().getStringExtra(PHONE_NUMBER);
        initResendOTPButton();
        findViewById(R.id.pageIndicator)
                .setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finish();
                                            Intent intent = new Intent(VerifyActivationCodeActivity.this,
                                                    WelcomeActivity.class);
                                            startActivity(intent);
                                        }
                                    }
                );
    }

    private void initBottomImageSlideInAnimation() {
//        final ImageView bottomImage = findViewById(R.id.bottomImage);
//        bottomImage.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_bottom));
    }

    private void initOtpEditText() {
        pinEntry = findViewById(R.id.editTextActivationCode);
        if (pinEntry != null) {
            pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {
                    String pin = str.toString();
                    if (pin.length() == 4) {
                        getLocationGPSandMakeRequest(pin);
                    }
                }
            });
        }
    }

    private void initResendOTPButton() {
        findViewById(R.id.buttonReturnActivationCode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProgressHandler progressHandler = new ProgressHandler(VerifyActivationCodeActivity.this);
                StringRequest request = getAddUserStep1Request(phoneNumber, progressHandler);
                request.setShouldCache(false);
                VolleySingleton.getInstance(getApplicationContext())
                        .addToRequestQueue(request);
            }
        });
    }

    private StringRequest getAddUserStep1Request(final String phoneNumber, final ProgressHandler progressHandler) {
        return new StringRequest(Request.Method.POST, BASEURL + ADD_USER_STEP1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            Boolean success = jsonResponse.getBoolean("success");
                            JSONObject result = jsonResponse.getJSONObject("result");
                            Boolean newUser = result.getBoolean("new");
                            String token = result.getString("token");
                            FflokDataSingleton.getInstance().setToken(token, getApplicationContext());
                            FflokDataSingleton.getInstance().setNewUser(newUser);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressHandler.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getApplicationContext());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("phone", phoneNumber);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-api-key", X_API_KEY);
                return params;
            }
        };
    }

    private void getLocationGPSandMakeRequest(String pin) {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != MockPackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST_LOCATION);
                // If any permission above not allowed by user, this condition will execute every time, else your else part will work
            } else {
                gps = new GPSTracker(VerifyActivationCodeActivity.this);
                // check if GPS enabled
                if (gps.canGetLocation()) {

                    latitude = String.valueOf(gps.getLatitude());
                    longitude = String.valueOf(gps.getLongitude());
                    ProgressHandler progressHandler = new ProgressHandler(this);
                    StringRequest request = addUserStep2VolleyRequest(pin, progressHandler);
                    request.setShouldCache(false);
                    VolleySingleton.getInstance(getApplicationContext())
                            .addToRequestQueue(request);
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocationGPSandMakeRequest(pinEntry.getText().toString());
                } else {
                    // Permission was not granted.
                    Toast.makeText(this, R.string.permission_denied_access_location, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private StringRequest addUserStep2VolleyRequest(final String code, final ProgressHandler progressHandler) {
        return new StringRequest(Request.Method.PUT, BASEURL + ADD_USER_STEP2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:vericode", "" + response);

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONObject result = jsonResponse.getJSONObject("result");
                            AddUserStep2Response addUserStep2Response = new Gson().fromJson(result.toString(), AddUserStep2Response.class);
                            FflokDataSingleton.getInstance().setUser(addUserStep2Response.getUser());
                            FflokDataSingleton.getInstance().setUserId(addUserStep2Response.getUser().getId(), getApplicationContext());
                            finish();
                            if (addUserStep2Response.getUser().getUsername() == null
                                    || addUserStep2Response.getUser().getUsername().getText().isEmpty()) {
                                Intent intent = new Intent(VerifyActivationCodeActivity.this,
                                        WelcomeActivity.class);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(VerifyActivationCodeActivity.this,
                                        LinkContactsActivity.class);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressHandler.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getApplicationContext());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("code", code);
                params.put("registrationToken", FirebaseInstanceId.getInstance().getToken());
                params.put("latitude", latitude);
                params.put("longitude", longitude);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getApplicationContext()));
                return headers;
            }
        };
    }

//    @Override
//    public void onLocationChanged(Location location) {
//        // Called when a new location is found by the network location provider.
//        latitude = Double.toString(location.getLatitude());
//        longitude = Double.toString(location.getLongitude());
//        locationManager.removeUpdates(this);
//        StringRequest request = addUserStep2VolleyRequest(pinEntry.getText().toString());
//        request.setShouldCache(false);
//        VolleySingleton.getInstance(getApplicationContext())
//                .addToRequestQueue(request);
//    }
//
//    @Override
//    public void onStatusChanged(String provider, int status, Bundle extras) {
//
//    }
//
//    @Override
//    public void onProviderEnabled(String provider) {
//
//    }
//
//    @Override
//    public void onProviderDisabled(String provider) {
//
//    }
}
