package com.orevon.fflok.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.orevon.fflok.R;
import com.orevon.fflok.models.Fflok;
import com.orevon.fflok.utils.AndroidUtils;
import com.orevon.fflok.utils.CommonUtils;
import com.orevon.fflok.utils.FflokDataSingleton;
import com.orevon.fflok.utils.internet.NetworkUtils;
import com.orevon.fflok.utils.internet.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.orevon.fflok.activity.VerifyActivationCodeActivity.PHONE_NUMBER;
import static com.orevon.fflok.constants.ApiPaths.ADD_USER_STEP1;
import static com.orevon.fflok.constants.ApiPaths.BASEURL;
import static com.orevon.fflok.constants.FflokConstants.X_API_KEY;

public class LoginActivity extends AppCompatActivity {

    private EditText editTextCountryCode;
    private EditText editTextPhoneNumber;
    private Button buttonSubmitPhoneNumber;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        animateViews();
        initControls();
//        findViewById(R.id.pageIndicator)
//                .setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View view) {
//                                            finish();
//                                            Intent intent = new Intent(LoginActivity.this,
//                                                    VerifyActivationCodeActivity.class);
//                                            intent.putExtra(OTP, otp);
//                                            startActivity(intent);
//                                        }
//                                    }
//                );
    }

    private void animateViews() {
        findViewById(R.id.bottomCenterBg).startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_in_bottom));
        findViewById(R.id.bottomLeftBg).startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_in_left));
        findViewById(R.id.bottomRightBg).startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_in_right));
        final ImageView logo = findViewById(R.id.logo);

        final Context context = this;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ResizeAnimation resizeAnimation = new ResizeAnimation(
                        logo,
                        600, AndroidUtils.dpToPx(context, 54), 600, AndroidUtils.dpToPx(context, 54));
                resizeAnimation.setDuration(500);
                logo.startAnimation(resizeAnimation);
            }
        }, 100);


    }

    private void initControls() {
        editTextCountryCode = findViewById(R.id.editTextCountryCode);
        editTextPhoneNumber = findViewById(R.id.editTextPhoneNumber);
        editTextPhoneNumber.requestFocus();
        initSubmitButton();
        progressBar = findViewById(R.id.progress);
    }

    private void initSubmitButton() {
        buttonSubmitPhoneNumber = findViewById(R.id.buttonSubmitPhoneNumber);
        buttonSubmitPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin(editTextPhoneNumber.getText().toString());
            }
        });
    }

    private void showProgress() {
        editTextPhoneNumber.setEnabled(false);
        buttonSubmitPhoneNumber.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.bringToFront();
    }

    private void hideProgress() {
        editTextPhoneNumber.setEnabled(true);
        buttonSubmitPhoneNumber.setEnabled(true);
        progressBar.setVisibility(View.GONE);
    }

    private void attemptLogin(final String phoneNumber) {
        if (!NetworkUtils.isOnline(getApplicationContext())) {
            AndroidUtils.showNoInternetSnackBar(this);
            return;
        }

        editTextPhoneNumber.setError(null);

        if (!validate()) {
            return;
        }

        if(editTextPhoneNumber.getText().toString().length() > 0) {
            showProgress();
            StringRequest request = getAddUserStep1Request(phoneNumber);
            request.setShouldCache(false);
            VolleySingleton.getInstance(getApplicationContext())
                    .addToRequestQueue(request);
        }
    }

    private boolean validate() {
        String number = editTextPhoneNumber.getText().toString();
        if (!TextUtils.isDigitsOnly(number)) {
            editTextPhoneNumber.setError("Invalid phone number");
            return false;
        }
        return true;
    }

//    private StringRequest getVolleyRequest(final String phoneNumber) {
//        return new StringRequest(Request.Method.POST, LOGIN,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.v("Volley:Response ", "" + response);
//                        hideProgress();
//                        LoginResponse lResponse = new Gson().fromJson(response, LoginResponse.class);
//                        if (lResponse.getErrorCode().equals("000")) {
//                            finish();
//                            Intent intent = new Intent(LoginActivity.this,
//                                    VerifyActivationCodeActivity.class);
//                            intent.putExtra(OTP, otp);
//                            intent.putExtra(PHONE_NUMBER, editTextCountryCode.getText().toString() + phoneNumber);
//                            startActivity(intent);
//                        } else {
////                            Toast.makeText(getApplicationContext(), "Error code: " + lResponse.getErrorCode(),
////                                    Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent(LoginActivity.this,
//                                    VerifyActivationCodeActivity.class);
//                            intent.putExtra(OTP, otp);
//                            startActivity(intent);
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        hideProgress();
//                        Log.v("Volley:ERROR ", error.getMessage());
////                        NetworkUtils.handleVolleyError(error);
//                    }
//                }
//        ) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                return CommonUtils.getParamsForOTPRequest(otp,
//                        editTextCountryCode.getText().toString() + phoneNumber);
//            }
//        };
//    }


    private StringRequest getAddUserStep1Request(final String phoneNumber) {
        return new StringRequest(Request.Method.POST, BASEURL + ADD_USER_STEP1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            Boolean success = jsonResponse.getBoolean("success");
                            JSONObject result = jsonResponse.getJSONObject("result");
                            Boolean newUser = result.getBoolean("new");
                            String token = result.getString("token");
                            FflokDataSingleton.getInstance().setToken(token, getApplicationContext());
                            FflokDataSingleton.getInstance().setNewUser(newUser);
                            finish();
                            Intent intent = new Intent(LoginActivity.this,
                                    VerifyActivationCodeActivity.class);
                            intent.putExtra(PHONE_NUMBER, editTextCountryCode.getText().toString() + phoneNumber);
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideProgress();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getApplicationContext());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("phone", editTextCountryCode.getText().toString() + phoneNumber);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
//                params.put("Content-Type","application/x-www-form-urlencoded");
                params.put("x-api-key", X_API_KEY);
                return params;
            }
        };
    }

    public class ResizeAnimation extends Animation {

        private int startWidth;
        private int deltaWidth; // distance between start and end height
        private int startHeight;
        private int deltaHeight;

        private int originalWidth;
        private int originalHeight;
        private View view;

        private boolean fillEnabled = false;


        public ResizeAnimation(View v, int startW, int endW, int startH, int endH) {
            view = v;
            startWidth = startW;
            deltaWidth = endW - startW;

            startHeight = startH;
            deltaHeight = endH - startH;

            originalHeight = v.getHeight();
            originalWidth = v.getWidth();
        }

        @Override
        public void setFillEnabled(boolean enabled) {
            fillEnabled = enabled;
            super.setFillEnabled(enabled);
        }


        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            if (interpolatedTime == 1.0 && !fillEnabled) {
                view.getLayoutParams().height = originalHeight;
                view.getLayoutParams().width = originalWidth;
            } else {
                if (deltaHeight != 0)
                    view.getLayoutParams().height = (int) (startHeight + deltaHeight * interpolatedTime);
                if (deltaWidth != 0)
                    view.getLayoutParams().width = (int) (startWidth + deltaWidth * interpolatedTime);
            }

            view.requestLayout();
        }
    }
}

