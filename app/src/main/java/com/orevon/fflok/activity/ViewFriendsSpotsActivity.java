package com.orevon.fflok.activity;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orevon.fflok.R;

public class ViewFriendsSpotsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_friends_spots);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view_spots);

        // use a linear layout manager
        StaggeredGridLayoutManager layoutManager = // new LinearLayoutManager(this);
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        // layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        recyclerView.setLayoutManager(layoutManager);
        layoutManager.setMeasurementCacheEnabled(false);
        recyclerView.addItemDecoration(new SpacesItemDecoration(16));
        recyclerView.addItemDecoration(new BottomLineDividerItemDecoration());

        SpotData[] dataSet = { // TODO: Test Data
                new SpotData("Le coin des copains", "Bordeaux"),
                new SpotData("La Sardine", "Talence "),
                new SpotData("Les planches", "Bordeaux"),
                new SpotData("Les Rotisseurs", "Pessac"),
        };
        SpotsViewAdapter adapter = new SpotsViewAdapter(dataSet);
        recyclerView.setAdapter(adapter);

        View buttonNext = findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ViewFriendsSpotsActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        animateHeaderAndFooter();
    }

    private void animateHeaderAndFooter() {
        View header = findViewById(R.id.header);
        View footer = findViewById(R.id.footer);

        header.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_top));
        footer.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_bottom));
    }
}

/**
 * Information about a spot
 */
class SpotData {
    private String name; // Name of the spot
    private String location; // Location

    public SpotData(String name, String location) {
        this.name = name;
        this.location = location;
    }

    public String getName() {
        return this.name;
    }

    public String getLocation() {
        return this.location;
    }
}


class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private final int space;

    public SpacesItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        outRect.bottom = 0;
        outRect.left = space;
        outRect.right = space;
        outRect.top = space;
        int pos = parent.getChildAdapterPosition(view);
        if (pos == 1) {
            outRect.top = 164;
        }
    }
}

class BottomLineDividerItemDecoration extends RecyclerView.ItemDecoration {
    public BottomLineDividerItemDecoration() {
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        if (parent.getChildAdapterPosition(view) == 0) {
            return;
        }
    }

    @Override
    public void onDraw(Canvas canvas, RecyclerView parent, RecyclerView.State state) {

        int dividerLeft = parent.getPaddingLeft();
        int dividerRight = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount - 2; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int startx = child.getLeft();
            int yOffset = (params.bottomMargin + params.topMargin) / 2;
            int y = child.getBottom() + yOffset;
            int endx = child.getRight();
            // TODO: Allocation should be moved out of onDraw(), for performance reasons
            Paint paint = new Paint();
            paint.setShader(new LinearGradient(0, 0, endx, 5,
                    Color.rgb(0x52, 0xac, 0xff),
                    Color.rgb(0x2b, 0xf3, 0x9b),
                    Shader.TileMode.MIRROR));
            paint.setStrokeWidth(5);
            canvas.drawLine(startx, y, endx, y, paint);
        }
    }
}

class SpotsViewAdapter extends RecyclerView.Adapter<SpotsViewAdapter.ViewHolder> {
    private SpotData[] mDataset;

    // Provide a suitable constructor (depends on the kind of dataset)
    public SpotsViewAdapter(SpotData[] myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SpotsViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.spot_card_view, parent, false);
        // set the view's size, margins, paddings and layout parameters

        View content = v.findViewById(R.id.spot_view_content);
        content.setClipToOutline(true);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        SpotData data = mDataset[position];

        // holder.mCardView.setData(mDataset[position]);
        TextView spotName = (TextView) holder.mCardView.findViewById(R.id.spot_name);
        spotName.setText(data.getName());

        TextView spotLocation = (TextView) holder.mCardView.findViewById(R.id.spot_location);
        spotLocation.setText(data.getLocation());
        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.mCardView.getLayoutParams();
        //layoutParams.setMargins(8, 8, 8, 8);

        // TODO: Revisit; a quick-n-dirty way to add some top margin to the second column
        // of images
        if (position == 1) {
            //layoutParams.setMargins(8, 8, 8, 8);
        }
        holder.mCardView.setLayoutParams(layoutParams);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout mCardView;

        public ViewHolder(LinearLayout view) {
            super(view);
            mCardView = view;
        }
    }
}


