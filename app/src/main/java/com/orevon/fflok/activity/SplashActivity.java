package com.orevon.fflok.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

import com.orevon.fflok.R;
import com.orevon.fflok.utils.AndroidUtils;
import com.orevon.fflok.utils.FflokDataSingleton;

/**
 * SplashActivity shows a splash screen, with changing background
 * image and logo.
 * <p>
 * Image change is done using two image switchers
 * - one for background, one for logo,
 * and changing them using a countdown timer
 */
public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_DURATION_MS = 5000;
    private static final int IMAGE_SWITCH_INTERVAL_MS = 1000;
    private static final int FADE_IN_DURATION = IMAGE_SWITCH_INTERVAL_MS;
    private static final int FADE_OUT_DURATION = IMAGE_SWITCH_INTERVAL_MS;

    private final int BACKGROUND_IMAGES[] = {
            R.drawable.splash_background1,
            R.drawable.splash_background2,
            R.drawable.splash_background3,
            R.drawable.splash_background4, // There was only 3 bg imgs available, hence reusing bg2
    };

    private int imgIndex = 0;
    private ImageSwitcher backgroundImageSwitcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidUtils.setTransparentStatusBar(getWindow());
        setContentView(R.layout.activity_splash);

        if(!FflokDataSingleton.getInstance().getToken(getApplicationContext()).equals("") && !FflokDataSingleton.getInstance().getUserId(getApplicationContext()).equals("")) {
            finish();
            Intent intent = new Intent(SplashActivity.this,
                    MainActivity.class);
            startActivity(intent);
        } else {
            Animation fadeIn = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
            Animation fadeOut = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
            fadeIn.setDuration(FADE_IN_DURATION);
            fadeOut.setDuration(FADE_OUT_DURATION);

            backgroundImageSwitcher = findViewById(R.id.backgroundImageSwitcher);
            backgroundImageSwitcher.setInAnimation(fadeIn);
            backgroundImageSwitcher.setOutAnimation(fadeOut);

            // Set the ViewFactory of the ImageSwitcher that will create ImageView object when asked
            backgroundImageSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
                public View makeView() {
                    ImageView imageView = new ImageView(getApplicationContext());
                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    imageView.setLayoutParams(new ImageSwitcher.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
                    return imageView;
                }
            });

            new CountDownTimer(SPLASH_DURATION_MS, IMAGE_SWITCH_INTERVAL_MS) {
                @Override
                public void onFinish() {
                    finish();
                    Intent intent = new Intent(SplashActivity.this, Intro1Activity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }

                // Switch to the next image
                @Override
                public void onTick(long millisUntilFinished) {
                    backgroundImageSwitcher.setImageResource(BACKGROUND_IMAGES[imgIndex]);
                    imgIndex = (imgIndex + 1) % BACKGROUND_IMAGES.length;
                }
            }.start();
        }
    }
}
