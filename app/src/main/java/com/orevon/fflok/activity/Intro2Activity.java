package com.orevon.fflok.activity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Path;
import android.graphics.drawable.GradientDrawable;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.orevon.fflok.R;
import com.orevon.fflok.handlers.ShakeEventHandler;
import com.orevon.fflok.listeners.ShakeEventListener;
import com.orevon.fflok.views.PeopleImageVideoView;

import pl.bclogic.pulsator4droid.library.PulsatorLayout;

import static android.content.Context.SENSOR_SERVICE;

public class Intro2Activity extends AppCompatActivity implements ShakeEventHandler {

    private static final String TAG = "IntroductionActivity";

    private ShakeEventListener mShakeListener;

    private ImageView vibratingPhoneIcon;

    private PeopleImageVideoView users[];
    private ColorMatrixColorFilter filter;
    private int random1 = 0, random2 = 7;

    private Handler handler = new Handler();
    private Handler showPeoplesHandler = new Handler();
    private int peoplesShownCount;
    private RelativeLayout peoplesContainer;

    private float xCoOrdinate;

    private ImageView user7SmallIcon, user4SmallIcon, user1SmallIcon, user13SmallIcon;

    ImageView handview;
    View textViewIntroduction3Text3;
    View continuerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_introduction3);
        initControls();
        initShakeListener();
    }

    private void initShakeListener() {
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (accelerometer != null) {
            mShakeListener = new ShakeEventListener(sensorManager, accelerometer);
        }
    }

    @Override
    public void handleShakeEvent() {
        handler.postDelayed(changeColorRunnable, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mShakeListener != null) {
            mShakeListener.registerHandler(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mShakeListener != null) {
            mShakeListener.unregisterHandler(this);
        }
    }

    private void initControls() {
        handview = findViewById(R.id.handview);
        vibratingPhoneIcon = findViewById(R.id.vibratingPhoneIcon);
        textViewIntroduction3Text3 = findViewById(R.id.textViewIntroduction3Text3);
        continuerButton = findViewById(R.id.buttonNext);
        peoplesContainer = findViewById(R.id.peoplesContainer);
        Button buttonPass = findViewById(R.id.buttonPass3);
        buttonPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLogin();
            }
        });
        PulsatorLayout pulsator = findViewById(R.id.pulsator);
        pulsator.start();
        initUsers();
        addDragEventToPeoplesContainer();
        showPeoplesHandler.postDelayed(showPeoplesRunnable, 0);
        findViewById(R.id.pageIndicator).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToLogin();
            }
        });
        findViewById(R.id.buttonNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLogin();
            }
        });

    }

    private void goToLogin() {
        finish();
        Intent intent = new Intent(Intro2Activity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }

    private void addDragEventToPeoplesContainer() {
        peoplesContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                        xCoOrdinate = view.getX() - event.getRawX();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        view.animate().x(event.getRawX() + xCoOrdinate).setDuration(0).start();
                        break;
                    case MotionEvent.ACTION_UP:
                        view.animate().x(0).setDuration(500).start();
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });
    }

    private void makePhoneIconVibrate() {
        ObjectAnimator animator = ObjectAnimator
                .ofFloat(vibratingPhoneIcon, "translationX",
                        0, 25, -25, 25, -25, 15, -15, 6, -6, 0);
        animator.setDuration(1000);
        animator.setRepeatCount(Animation.INFINITE);
        animator.start();
    }

    private void initUsers() {
        users = new PeopleImageVideoView[11];
        users[0] = findViewById(R.id.user6);
        users[1] = findViewById(R.id.user12);
        users[2] = findViewById(R.id.user3);
        users[3] = findViewById(R.id.user4);
        users[4] = findViewById(R.id.user2);
        users[5] = findViewById(R.id.user1);
        users[6] = findViewById(R.id.user13);
        users[7] = findViewById(R.id.user5);
        users[8] = findViewById(R.id.user14);
        users[9] = findViewById(R.id.user8);
        users[10] = findViewById(R.id.user7);

        user7SmallIcon = findViewById(R.id.user7SmallIcon);
        user4SmallIcon = findViewById(R.id.user4SmallIcon);
        user1SmallIcon = findViewById(R.id.user1SmallIcon);
        user13SmallIcon = findViewById(R.id.user13SmallIcon);

        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        filter = new ColorMatrixColorFilter(matrix);

        for (final PeopleImageVideoView view : users) {
            final PeopleImageVideoView pivv = (PeopleImageVideoView)view;
            pivv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pivv.playVideo();
                    GradientDrawable g = new GradientDrawable(GradientDrawable.Orientation.TL_BR, new int[]{Color.rgb(255, 255, 255), Color.rgb(255, 255, 255), Color.rgb(255, 255, 255), getResources().getColor(R.color.holowhite)});
                    g.setGradientType(GradientDrawable.RADIAL_GRADIENT);
                    g.setGradientRadius(((view).getHeight() / 2));
                    g.setCornerRadius((( view).getHeight() / 2));
                    (view).setBackground(g);
                }
            });
        }
    }

    private void randomlyChangeColor() {
        random1++;
        if (random1 > 10)
            random1 = 0;
        random2++;
        if (random2 > 10)
            random2 = 0;

        changeColor();
    }

    private void changeColor() {
        for (int i = 0; i < 11; i++) {
            if (i == random1 || i == random2) {
                users[i].profileImageView.clearColorFilter();
            } else {
                users[i].profileImageView.setColorFilter(filter);
            }
        }
    }

    private Runnable changeColorRunnable = new Runnable() {
        @Override
        public void run() {
            randomlyChangeColor();
//            handler.postDelayed(this, 3000);
        }
    };

    private Runnable showPeoplesRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                users[peoplesShownCount].setVisibility(View.VISIBLE);
                users[++peoplesShownCount].setVisibility(View.VISIBLE);
                users[++peoplesShownCount].setVisibility(View.VISIBLE);

                if (peoplesShownCount == 10) {
                    user7SmallIcon.setVisibility(View.VISIBLE);
                }

                if (peoplesShownCount == 5 || (peoplesShownCount - 1 == 5) || peoplesShownCount - 2 == 5) {
                    user1SmallIcon.setVisibility(View.VISIBLE);
                }

                if (peoplesShownCount == 6 || (peoplesShownCount - 1 == 6) || peoplesShownCount - 2 == 6) {
                    user13SmallIcon.setVisibility(View.VISIBLE);
                }

                if (peoplesShownCount == 3 || (peoplesShownCount - 1 == 3) || peoplesShownCount - 2 == 3) {
                    user4SmallIcon.setVisibility(View.VISIBLE);
                }

                if (peoplesShownCount < users.length - 1) {
                    showPeoplesHandler.postDelayed(this, 0);//if delay needed add here - between showing ppl
                } else {
//                    final Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
                            handview.setVisibility(View.VISIBLE);
                            handAnimator(5,handview,0,0);
//                        }
//                    }, 500);

                }

            } catch (IndexOutOfBoundsException e) {
                Log.d(TAG, "idx out of range");
            }

        }
    };

    public void handAnimator(final int i, final View current, final float fromx, final float fromy) {
        TranslateAnimation animation = new TranslateAnimation(fromx, (users[i].getX() - current.getX()) + 30, fromy, (users[i].getY() - current.getY()) + 200);
        animation.setRepeatMode(0);
        if(i==6) {
            animation.setDuration(4500);
        } else if(i==1){
            animation.setDuration(3500);
        } else if(i==5) {
            animation.setDuration(2000);
        }
        animation.setFillAfter(true);
        handview.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                users[i].performClick();
                if (i == 1) {
                    handAnimator(6,handview,(users[i].getX() - current.getX()) + 30,(users[i].getY() - current.getY()) + 200);
                } else if (i == 6) {
                    Log.e("sic","go man go");
                    handview.clearAnimation();
                    handview.setVisibility(View.GONE);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
//                            vibratingPhoneIcon.setVisibility(View.VISIBLE);
//                            textViewIntroduction3Text3.setVisibility(View.VISIBLE);
//                            makePhoneIconVibrate();
                            continuerButton.setVisibility(View.VISIBLE);
                            handview.setVisibility(View.VISIBLE);
                            handAnimator(5,handview,0,0);
                        }

                    }, 2000);

                } else if (i == 5) {
                    handAnimator(1,handview,(users[i].getX() - current.getX()) + 30,(users[i].getY() - current.getY()) + 200);
                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
}


