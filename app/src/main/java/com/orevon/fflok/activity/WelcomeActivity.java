package com.orevon.fflok.activity;

import android.animation.LayoutTransition;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.orevon.fflok.R;
import com.orevon.fflok.utils.CommonUtils;
import com.orevon.fflok.utils.FflokDataSingleton;
import com.orevon.fflok.utils.internet.NetworkUtils;
import com.orevon.fflok.utils.internet.VolleySingleton;
import com.orevon.fflok.views.ProgressHandler;

import java.util.HashMap;
import java.util.Map;

import static com.orevon.fflok.constants.FflokConstants.X_API_KEY;
import static com.orevon.fflok.constants.ApiPaths.ADD_USER_STEP3;
import static com.orevon.fflok.constants.ApiPaths.BASEURL;

public class WelcomeActivity extends AppCompatActivity {

    private ImageView imageCity, yellowBird;

    private TextView welcomeMsg1, welcomeMsg2;

    private EditText name;

    private RelativeLayout popup;

    private ConstraintLayout layout;
    private float mx;
    private float imageCityInitialScrollX, imageCityInitialScrollY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        initControls();
//        LinkContactsChoiceActivity
    }

    private void initControls() {
        layout = findViewById(R.id.layout);

        popup = findViewById(R.id.popup);

        imageCity = findViewById(R.id.imageCity);
        imageCity.setColorFilter(ResourcesCompat.getColor(getResources(), R.color.buildingTint, null),
                PorterDuff.Mode.SRC_IN);

        yellowBird = findViewById(R.id.yellowBird);

        welcomeMsg1 = findViewById(R.id.welcomeMsg1);
        welcomeMsg2 = findViewById(R.id.welcomeMsg2);

        name = findViewById(R.id.name);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/CircularStd-Bold.otf");
        name.setTypeface(tf);

        findViewById(R.id.popupClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.setVisibility(View.GONE);
            }
        });

        addAnimations();
//        makeCityDraggable();
        showWelcomeMessages();

        name.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    gotoLinkContactsActivity();
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
        findViewById(R.id.pageIndicator).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoLinkContactsActivity();
            }
        });

    }

    private void gotoLinkContactsActivity() {
        if(FflokDataSingleton.getInstance().getNewUser()) {
            if(name.getText().toString().length() > 0) {
                callAddUserStep3Request();
            }
        } else {
            if(name.getText().toString().length() == 0) {
                finish();
                Intent intent = new Intent(WelcomeActivity.this,
                        LinkContactsActivity.class);
                startActivity(intent);
            } else {
                callAddUserStep3Request();
            }
        }
    }

    private void callAddUserStep3Request() {
        ProgressHandler progressHandler = new ProgressHandler(WelcomeActivity.this);
        StringRequest request = getAddUserStep3VolleyRequest(name.getText().toString(), progressHandler);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getApplicationContext())
                .addToRequestQueue(request);
    }

    private void showWelcomeMessages() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                layout.setLayoutTransition(new LayoutTransition());
                welcomeMsg1.setVisibility(View.VISIBLE);
            }
        }, 5500);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                welcomeMsg1.setVisibility(View.GONE);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
                        welcomeMsg2.setVisibility(View.VISIBLE);
                        name.setVisibility(View.VISIBLE);
//                    }
//                }, 1000);
            }
        }, 8000);
    }

    private void addAnimations() {
//        if(!FflokDataSingleton.getInstance().getNewUser()) {
            popup.setVisibility(View.VISIBLE);
//            popup.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.welcome_popup_anim));
//        }
        imageCity.setVisibility(View.VISIBLE);
//        imageCity.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_right));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                yellowBird.setVisibility(View.VISIBLE);
                yellowBird.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left));
                imageCityInitialScrollX = imageCity.getScrollX();
                imageCityInitialScrollY = imageCity.getScrollY();
            }
        }, 2500);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                yellowBird.setImageResource(R.drawable.yellow_bird_eye_blink);
                final AnimationDrawable frameAnimation = (AnimationDrawable) yellowBird.getDrawable();
                frameAnimation.start();
            }
        }, 3000);
    }

    // city image can be dragged horizontally, and comes to origin position on touch release
    private void makeCityDraggable() {
        imageCity.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                float curX;
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mx = event.getX();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        curX = event.getX();
                        imageCity.scrollBy((int) (mx - curX), 0);
                        mx = curX;
                        break;
                    case MotionEvent.ACTION_UP:
                        imageCity.scrollTo((int) (imageCityInitialScrollX), (int) (imageCityInitialScrollY));
                        break;
                }
                return true;
            }
        });
    }

    private StringRequest getAddUserStep3VolleyRequest(final String firstName, final ProgressHandler progressHandler) {
        return new StringRequest(Request.Method.PUT, BASEURL + ADD_USER_STEP3,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        FflokDataSingleton.getInstance().getUser().getUsername().setText(firstName); //abhi commented for crash
//                        if(FflokDataSingleton.getInstance().getNewUser()) {
                            popup.setVisibility(View.VISIBLE);
//                            popup.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.welcome_popup_anim));
//                        }
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressHandler.hide();
                                finish();
                                Intent intent = new Intent(WelcomeActivity.this,
                                        LinkContactsActivity.class);
                                startActivity(intent);
                            }
                        }, 2000);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getApplicationContext());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("username",firstName);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getApplicationContext()));
                return headers;
            }
        };
    }
}
