package com.orevon.fflok.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.vivchar.viewpagerindicator.ViewPagerIndicator;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.orevon.fflok.R;
import com.orevon.fflok.adapters.CustomPagerAdapter;
import com.orevon.fflok.adapters.PeoplesAdapter;
import com.orevon.fflok.adapters.PlaceListAdapter;
import com.orevon.fflok.adapters.TitlesAdapter;
import com.orevon.fflok.models.Place;
import com.orevon.fflok.models.Title;
import com.orevon.fflok.utils.CommonUtils;
import com.orevon.fflok.utils.FflokDataSingleton;
import com.orevon.fflok.utils.internet.NetworkUtils;
import com.orevon.fflok.utils.internet.VolleySingleton;
import com.orevon.fflok.views.ProgressHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.orevon.fflok.constants.ApiPaths.BASEURL;
import static com.orevon.fflok.constants.ApiPaths.CREATE_FFLOK;
import static com.orevon.fflok.constants.ApiPaths.FIND_TAGS_FFLOK;
import static com.orevon.fflok.constants.ApiPaths.GET_TAGS_FFLOK;
import static com.orevon.fflok.constants.ApiPaths.UPDATE_PLACES_FFLOK;
import static com.orevon.fflok.constants.FflokConstants.X_API_KEY;

public class ViewFussActivity extends AppCompatActivity implements View.OnClickListener, TitlesAdapter.TitleClickedCallBack, PlaceListAdapter.PlaceClickedCallBack {

    private TextView timeTV;
    private ImageView privateIV;
    private TextView privateTV;
    private ImageView vibrateIcon;
    private TextView vibrateText;
    private TextView titleTV;
    private EditText titleEditText;
    private Button nextButton;
    private ConstraintLayout layout1;
    private ConstraintLayout layout2;
    private RecyclerView eventsList;
    private RecyclerView placesListRecyclerView;

    private Calendar time;
    private boolean isPrivate = true;
    private boolean isVibrate = true;
    private boolean isTitlesShown = false;
    private int selectedEventPos = 0;
    private List<Title> titles = new ArrayList<>();
    private List<Title> originalTitles = new ArrayList<>();
    private List<String> selectedPlaceIds = new ArrayList<>();
    private List<String> selectedPlacePositions = new ArrayList<>();
    TitlesAdapter titlesAdapter;
    PlaceListAdapter placeListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_fuss);
        initUI();
        callGetTagsAPI();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.time:
                handleTimeFieldClick();
                break;
            case R.id.oval2:
                handleIsPrivateClick();
                break;
            case R.id.oval3:
                handleVibrateClick();
                break;
            case R.id.title:
                handleTitleClick();
                break;
            case R.id.buttonNext:
                handleNextButtonClick();
        }
    }



    private void initUI() {
        initToolbar();
        initPeoples();
//        initViewPager();
        initTimeField();
        initPrivateFields();
        initVibrateFields();
        initTitle();
        initPlacesList();
    }

    private void initTitle() {
        titleTV = findViewById(R.id.title);
        titleEditText = findViewById(R.id.titleEditText);
        titleTV.setOnClickListener(this);
        titleTV.setVisibility(View.VISIBLE);
        titleEditText.setVisibility(View.GONE);
        layout1 = findViewById(R.id.layout1);
        layout2 = findViewById(R.id.layout2);
        eventsList = findViewById(R.id.list);
        nextButton = findViewById(R.id.buttonNext);
//        titleEditText.setOnKeyListener(new View.OnKeyListener() {
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
//                    callFindTagsAPI();
//                    return true;
//                }
//                return false;
//            }
//        });
        titleEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    callFindTagsAPI();
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
    }

    private void initVibrateFields() {
        vibrateIcon = findViewById(R.id.vibrateIcon);
        vibrateText = findViewById(R.id.vibrateText);
        findViewById(R.id.oval3).setOnClickListener(this);
    }

    private void initPrivateFields() {
        privateIV = findViewById(R.id.openLock);
        privateTV = findViewById(R.id.privateText);
        findViewById(R.id.oval2).setOnClickListener(this);
    }

    private void initTimeField() {
        timeTV = findViewById(R.id.time);
        timeTV.setOnClickListener(this);

        time = Calendar.getInstance();
        time.setTime(new Date());
        time.set(Calendar.HOUR_OF_DAY, 12);
        time.set(Calendar.MINUTE, 30);

        setTimeIntoTimeTV();
    }

    private void setTimeIntoTimeTV() {
        int hr = time.get(Calendar.HOUR_OF_DAY), min = time.get(Calendar.MINUTE);
        String t = (hr < 10 ? "0" + hr : hr) + "h" + (min < 10 ? "0" + min : min);
        timeTV.setText(t);
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void initPeoples() {
        RecyclerView peoples = findViewById(R.id.peoples);
        peoples.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, true));
        peoples.setAdapter(new PeoplesAdapter(this));
    }

    private void initViewPager() {
        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(new CustomPagerAdapter(this));

        ViewPagerIndicator viewPagerIndicator = findViewById(R.id.view_pager_indicator);
        viewPagerIndicator.setupWithViewPager(viewPager);
//        viewPagerIndicator.addOnPageChangeListener(mOnPageChangeListener);
    }

    private void initPlacesList() {
        placesListRecyclerView = findViewById(R.id.places_list_recycler_view);
        placesListRecyclerView.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(this);
        MyLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        if (FflokDataSingleton.getInstance().getProximatePlaces() != null && FflokDataSingleton.getInstance().getProximatePlaces().size() > 0 & placesListRecyclerView != null) {
            placeListAdapter = new PlaceListAdapter(getApplicationContext(), FflokDataSingleton.getInstance().getProximatePlaces());
            placeListAdapter.setCallback(this);
            placesListRecyclerView.setAdapter(placeListAdapter);
        }
        placesListRecyclerView.setLayoutManager(MyLayoutManager);
    }

    private void handleTimeFieldClick() {
        time.add(Calendar.MINUTE, 30);
        setTimeIntoTimeTV();
    }

    private void handleIsPrivateClick() {
        isPrivate = !isPrivate;
        privateIV.setImageResource(isPrivate ? R.drawable.closelock : R.drawable.openlock);
        privateTV.setText(isPrivate ? R.string._private : R.string.open);
    }

    private void handleVibrateClick() {
        isVibrate = !isVibrate;
        if (isVibrate) {
            vibrateIcon.setImageResource(R.drawable.vibrate_phone);
            vibrateText.setText("2 nouvelles\npropositions");
            vibrateText.setGravity(Gravity.START);
        } else {
            vibrateIcon.setImageResource(R.drawable.ic_back);
            vibrateText.setText("propositions\nprécédentes");
            vibrateText.setGravity(Gravity.END);
        }
    }

    private void handleTitleClick() {
        isTitlesShown = !isTitlesShown;
        if (isTitlesShown) {
            layout2.setVisibility(View.VISIBLE);
            layout1.setVisibility(View.GONE);
            titleTV.setVisibility(View.INVISIBLE);
            titleEditText.setVisibility(View.VISIBLE);
            eventsList.setLayoutManager(new LinearLayoutManager(this));
            titles = originalTitles;
            titlesAdapter = new TitlesAdapter(this, titles, selectedEventPos);
            titlesAdapter.setCallback(ViewFussActivity.this);
            eventsList.setAdapter(titlesAdapter);
        } else {
            layout2.setVisibility(View.GONE);
            layout1.setVisibility(View.VISIBLE);
            titleTV.setVisibility(View.VISIBLE);
            titleEditText.setVisibility(View.GONE);
        }
    }

    private void handleNextButtonClick() {
        if(selectedPlaceIds.size() == 2) {
            callUpdatePlacesAPI();
        } else {
            Toast.makeText(getApplicationContext(), "Please select 2 places", Toast.LENGTH_SHORT).show();
        }
    }

    private String getSelectedPlacesJson() {
        JSONObject place1 = new JSONObject();
        JSONObject place2 = new JSONObject();

        try {
            place1.put("id", selectedPlaceIds.get(0));
            place2.put("id", selectedPlaceIds.get(1));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(place1);
        jsonArray.put(place2);
        return jsonArray.toString();
    }

    private void callGetTagsAPI() {
        ProgressHandler progressHandler = new ProgressHandler(ViewFussActivity.this);
        StringRequest request = getTagsVolleyRequest(progressHandler);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void callFindTagsAPI() {
        ProgressHandler progressHandler = new ProgressHandler(ViewFussActivity.this);
        StringRequest request = findTagsVolleyRequest(progressHandler);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private void callUpdatePlacesAPI() {
        ProgressHandler progressHandler = new ProgressHandler(ViewFussActivity.this);
        StringRequest request = getUpdatePlacesVolleyRequest(progressHandler);
        request.setShouldCache(false);
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }

    private StringRequest getTagsVolleyRequest(final ProgressHandler progressHandler) {
        return new StringRequest(Request.Method.GET, BASEURL + GET_TAGS_FFLOK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonarray = jsonResponse.getJSONArray("titles ");
                            titles.clear();
                            for(int i = 0; i < jsonarray.length(); i++) {
                                Gson gson = new Gson();
                                Title title = gson.fromJson(jsonarray.get(i).toString(), Title.class);
                                titles.add(title);
                            }
//                            originalTitles = titles;
                            originalTitles.clear();
                            originalTitles.addAll(titles);
//                            for (Title title: titles) {
//                                originalTitles.add(title);
//                            }
                            if(titles.size() > 0) {
                                String titleText = "#" +titles.get(0).getText();
                                titleTV.setText(titleText);
                                titleEditText.setText(titleText);
//                                titleClicked(0, titles.get(0));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressHandler.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getApplicationContext());
                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getApplicationContext()));
                return headers;
            }
        };
    }

    private StringRequest getUpdatePlacesVolleyRequest(final ProgressHandler progressHandler) {
        return new StringRequest(Request.Method.PUT, BASEURL + UPDATE_PLACES_FFLOK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response  ", "getUpdatePlacesVolleyRequest-->" + response);
                       //{"success":true,"fflok":{"statu":"closed","_id":"5b4c96bc72bed37bd9677352","images":[],"messages":[],"friends":[{"acepte":true,"vote":false,"date":"2018-07-16T12:59:40.500Z","_id":"5b4c96bc72bed37bd9677353","id":{"prenon":{"text":"arun","visible":true},"active":true,"démocrate":100,"number_fflok":0,"statu":true,"lang":"FR","_id":"5b39b813109d274232a5e495","phone":"+919526670808","code":"8972","path_img":[],"adresse":[],"age":[],"email":[],"fflok":[],"likers":[],"favorite_place":[],"badge":[{"date":"2018-07-02T05:29:59.928Z","_id":"5b39b857109d274232a5e496","name":"Nouvelle recrue ","subtitle":""}],"create_date":"2018-07-02T05:28:51.859Z","__v":0,"geo":[8.5478135,76.8791473],"latitude":"8.5478135","longitude":"76.8791473","registrationToken":"cjKlrLdjk74:APA91bF9q1f1VuVsUBy6lY9m3-iSJ6joA1c0U5pPKgF4VdGlS3f7vd1vaDSGPCT69AyIZ618UKSIXwlYe3j6WTFP6fJPUZ84oXwHdiVxlmMDynp8Gzi4G2NsnRAtVJfCRgCCNg10dV97S2xYgzkAVdvmNDCPewKTQg"}}],"places":[],"create_date":"2018-07-16T12:59:40.500Z","create_by":{"prenon":{"text":"Jincy\n","visible":true},"active":true,"démocrate":100,"number_fflok":0,"statu":null,"lang":"FR","_id":"5b2bd9d8accde76272d1969e","phone":"+919995400596","code":"7299","path_img":[{"_id":"5b360d3e109d274232a5e48f","path":"video-2018-06-29-file.mp4","visible":true}],"adresse":[],"age":[],"email":[],"fflok":[{"acepte":true,"vote":true,"date":"2018-07-14T08:51:43.912Z","_id":"5b49b99f6d50766fc58bca45","id":"5b49b99f6d50766fc58bca43","create_by_me":true},{"acepte":true,"vote":true,"date":"2018-07-14T08:55:13.772Z","_id":"5b49ba716d50766fc58bca48","id":"5b49ba716d50766fc58bca46","create_by_me":true},{"acepte":true,"vote":true,"date":"2018-07-14T09:04:39.690Z","_id":"5b49bca76d50766fc58bca4b","id":"5b49bca76d50766fc58bca49","create_by_me":true},{"acepte":true,"vote":true,"date":"2018-07-14T09:16:47.179Z","_id":"5b49bf7f20f0fb03ee8fac88","id":"5b49bf7f20f0fb03ee8fac86","create_by_me":true},{"acepte":true,"vote":true,"date":"2018-07-14T09:23:24.642Z","_id":"5b49c10cd475e3046b7e3b8e","id":"5b49c10cd475e3046b7e3b8c","create_by_me":true},{"acepte":true,"vote":true,"date":"2018-07-14T09:31:15.515Z","_id":"5b49c2e3d475e3046b7e3b91","id":"5b49c2e3d475e3046b7e3b8f","create_by_me":true},{"acepte":true,"vote":true,"date":"2018-07-14T09:34:44.039Z","_id":"5b49c3b4d475e3046b7e3b94","id":"5b49c3b4d475e3046b7e3b92","create_by_me":true},{"acepte":true,"vote":true,"date":"2018-07-16T09:55:45.383Z","_id":"5b4c6ba17c6bb07950de42dd","id":"5b4c6ba17c6bb07950de42db","create_by_me":true},{"acepte":true,"vote":true,"date":"2018-07-16T09:55:56.347Z","_id":"5b4c6bac7c6bb07950de42e0","id":"5b4c6bac7c6bb07950de42de","create_by_me":true},{"acepte":true,"vote":true,"date":"2018-07-16T09:56:10.433Z","_id":"5b4c6bba7c6bb07950de42e3","id":"5b4c6bba7c6bb07950de42e1","create_by_me":true},{"acepte":true,"vote":true,"date":"2018-07-16T10:11:30.236Z","_id":"5b4c6f5272bed37bd9677345","id":"5b4c6f5272bed37bd9677343","create_by_me":true},{"acepte":true,"vote":true,"date":"2018-07-16T11:40:45.840Z","_id":"5b4c843d72bed37bd967734a","id":"5b4c843d72bed37bd9677348","create_by_me":true},{"acepte":true,"vote":true,"date":"2018-07-16T12:57:38.852Z","_id":"5b4c964272bed37bd9677351","id":"5b4c964272bed37bd967734f","create_by_me":true},{"acepte":true,"vote":true,"date":"2018-07-16T12:59:40.503Z","_id":"5b4c96bc72bed37bd9677354","id":"5b4c96bc72bed37bd9677352","create_by_me":true}],"likers":[{"like":true,"score":0,"date":"2018-06-26T13:22:20.200Z","_id":"5b323e0ce8287b1286eabcf2","id":"5b2bd9d8accde76272d1969e"},{"like":true,"score":0,"date":"2018-06-26T13:24:05.173Z","_id":"5b323e75e8287b1286eabcf3","id":"5b2bd9d8accde76272d1969e"},{"like":true,"score":0,"date":"2018-06-26T13:29:20.666Z","_id":"5b323fb0e8287b1286eabcf4","id":"5b2bd9d8accde76272d1969e"},{"like":true,"score":0,"date":"2018-06-27T06:25:36.503Z","_id":"5b332de0e8287b1286eabcf7","id":"5b2bd9d8accde76272d1969e"},{"like":true,"score":0,"date":"2018-06-27T06:25:39.451Z","_id":"5b332de3e8287b1286eabcf8","id":"5b2bd9d8accde76272d1969e"},{"like":true,"score":0,"date":"2018-07-10T11:31:19.738Z","_id":"5b449907f7914726aff7bec4","id":"5b39b813109d274232a5e495"},{"like":true,"score":0,"date":"2018-07-10T11:38:10.394Z","_id":"5b449aa2f7914726aff7bec5","id":"5b30c20be650ab5a3d3392d5"},{"like":true,"score":0,"date":"2018-07-10T12:19:39.498Z","_id":"5b44a45b9b7664360c3225fc","id":"5b4094ebb9f085716f71456c"},{"like":true,"score":0,"date":"2018-07-16T09:51:56.827Z","_id":"5b4c6abc7c6bb07950de42d4","id":"5b4c6abc7c6bb07950de42d3"},{"like":true,"score":0,"date":"2018-07-16T09:52:14.788Z","_id":"5b4c6ace7c6bb07950de42d6","id":"5b4c6ace7c6bb07950de42d5"},{"like":true,"score":0,"date":"2018-07-16T09:52:59.465Z","_id":"5b4c6afb7c6bb07950de42d8","id":"5b4c6afb7c6bb07950de42d7"},{"like":true,"score":0,"date":"2018-07-16T09:53:29.041Z","_id":"5b4c6b197c6bb07950de42da","id":"5b4c6b197c6bb07950de42d9"}],"favorite_place":[],"badge":[{"date":"2018-06-25T12:18:16.171Z","_id":"5b30dd88e650ab5a3d3392d6","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-06-25T12:37:12.703Z","_id":"5b30e1f8e650ab5a3d3392d7","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-06-25T12:39:11.027Z","_id":"5b30e26fe650ab5a3d3392d8","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-06-27T05:28:57.610Z","_id":"5b332099e8287b1286eabcf6","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-06-28T18:48:08.216Z","_id":"5b352d68109d274232a5e48a","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-06-28T18:51:01.962Z","_id":"5b352e15109d274232a5e48b","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-06-28T19:01:05.723Z","_id":"5b353071109d274232a5e48c","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-06-29T09:10:12.159Z","_id":"5b35f774109d274232a5e48d","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-06-29T09:14:31.387Z","_id":"5b35f877109d274232a5e48e","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-06-30T10:52:56.367Z","_id":"5b376108109d274232a5e490","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-06-30T10:59:06.087Z","_id":"5b37627a109d274232a5e491","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-06-30T11:06:21.082Z","_id":"5b37642d109d274232a5e492","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-07-01T09:04:24.779Z","_id":"5b389918109d274232a5e493","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-07-01T09:19:09.675Z","_id":"5b389c8d109d274232a5e494","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-07-02T08:53:24.687Z","_id":"5b39e804109d274232a5e497","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-07-02T08:53:27.864Z","_id":"5b39e807109d274232a5e498","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-07-02T09:09:58.173Z","_id":"5b39ebe6109d274232a5e499","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-07-02T09:20:03.403Z","_id":"5b39ee43109d274232a5e49a","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-07-02T09:42:33.269Z","_id":"5b39f389109d274232a5e49d","name":"Nouvelle recrue ","subtitle":""},{"date":"2018-07-10T11:30:33.857Z","_id":"5b4498d9f7914726aff7bec3","name":"Nouvelle recrue ","subtitle":""}],"create_date":"2018-06-21T17:01:12.579Z","__v":0,"geo":[8.5478137,76.8791453],"latitude":"8.5478137","longitude":"76.8791453","registrationToken":"fVuwhAxsZHs:APA91bELLRNA8jUeLANrh00FKVIS5CCjFAetiODw5om37-HR7WzaXXqZorWQYKxbNy_EbrmQtcyheKD4LNNPcbEGK5wZXyCmG20zzGvv7kZvtBhCrU2mftsDGQB6Ti8yYoZf4eahq3LMKRLvYc9LTK8QqE5tRRLC0g"},"__v":0}}
                        try {
                            JSONObject result = new JSONObject(response);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressHandler.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getApplicationContext());
                    }
                }
        ) {

//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                HashMap<String, String> params = new HashMap<String, String>();
//                params.put("title", titles.get(selectedEventPos).getText());
//                params.put("idfflok", FflokDataSingleton.getInstance().getFflokId());
////                params.put("places", getSelectedPlacesJson());
//                params.put("places", getPlacesString());
//                return params;
//            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getApplicationContext()));
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                HashMap<String, Object> params = new HashMap<String, Object>();

//                params.put("places", getPlacesString());

//                String[] array1 = selectedPlaceIds.toArray(new String[selectedPlaceIds.size()]);
//                params.put("places", array1);

                JSONArray json1 = new JSONArray(selectedPlaceIds);
                params.put("places", json1);
                params.put("title", titles.get(selectedEventPos).getText());
                params.put("idGloo", FflokDataSingleton.getInstance().getFflokId());
                params.put("statusGloo", 1); //todo: Temporary implementation. should be dynamic
                params.put("timeGloo", new SimpleDateFormat("h:MM", Locale.ENGLISH).format(Calendar.getInstance().getTime()));
                return new JSONObject(params).toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
    }

    private String getPlacesString() {
        String json = getSelectedPlacesJson();
        json = json.replaceAll("\"id\"", "id");
        return json;


    }

    private StringRequest findTagsVolleyRequest(final ProgressHandler progressHandler) {
        return new StringRequest(Request.Method.POST, BASEURL + FIND_TAGS_FFLOK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonarray = jsonResponse.getJSONArray("titles ");
                            titles.clear();
                            for(int i = 0; i < jsonarray.length(); i++) {
                                Gson gson = new Gson();
                                Title title = gson.fromJson(jsonarray.get(i).toString(), Title.class);
                                titles.add(title);
                            }
                            if(titles.size() > 0) {
                                String titleText = "#" +titles.get(0).getText();
                                titleTV.setText(titleText);
                                titleEditText.setText(titleText);
//                                titleClicked(0, titles.get(0));
                            }
                            titlesAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressHandler.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getApplicationContext());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                String text = titleEditText.getText().toString();
                if(text.startsWith("#")) {
                    text = text.substring(1);
                }
                params.put("text", text);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getApplicationContext()));
                return headers;
            }
        };
    }

    @Override
    public void titleClicked(int position, Title title) {
        String titleText = "#" +title.getText();
        titleTV.setText(titleText);
        titleEditText.setText(titleText);
        layout2.setVisibility(View.GONE);
        layout1.setVisibility(View.VISIBLE);
        titleTV.setVisibility(View.VISIBLE);
        titleEditText.setVisibility(View.GONE);
        selectedEventPos = originalTitles.indexOf(title);
//        selectedEventPos = position;
        isTitlesShown = false;
    }

    @Override
    public void placeClicked(int position, String placeId) {
        if(selectedPlaceIds.contains(placeId)) {
            selectedPlaceIds.remove(placeId);
            selectedPlacePositions.remove(String.valueOf(position));
            FflokDataSingleton.getInstance().getProximatePlaces().get(position).setSelected(false);
        } else if(selectedPlaceIds.size() == 2) {
            selectedPlaceIds.remove(0);
            int removedPos =  Integer.valueOf(selectedPlacePositions.get(0));
            FflokDataSingleton.getInstance().getProximatePlaces().get(removedPos).setSelected(false);
            selectedPlacePositions.remove(0);
            selectedPlaceIds.add(placeId);
            selectedPlacePositions.add(String.valueOf(position));
            FflokDataSingleton.getInstance().getProximatePlaces().get(position).setSelected(true);
        } else {
            selectedPlaceIds.add(placeId);
            selectedPlacePositions.add(String.valueOf(position));
            FflokDataSingleton.getInstance().getProximatePlaces().get(position).setSelected(true);
        }
        placeListAdapter.notifyDataSetChanged();
    }
}
