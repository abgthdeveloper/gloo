package com.orevon.fflok.activity;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.orevon.fflok.R;
import com.orevon.fflok.adapters.ContactRecordAdapter;
import com.orevon.fflok.fragments.LinkContactPopupFragment;
import com.orevon.fflok.models.ApiResponse.AddUserFriendResponse;
import com.orevon.fflok.models.ContactRecord;
import com.orevon.fflok.models.FBFriend;
import com.orevon.fflok.models.UserContactFeature;
import com.orevon.fflok.utils.CommonUtils;
import com.orevon.fflok.utils.FflokDataSingleton;
import com.orevon.fflok.utils.internet.NetworkUtils;
import com.orevon.fflok.utils.internet.VolleySingleton;
import com.orevon.fflok.views.ProgressHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import static com.orevon.fflok.constants.ApiPaths.ADD_FRIEND;
import static com.orevon.fflok.constants.ApiPaths.CHECK_USERS;
import static com.orevon.fflok.constants.FflokConstants.X_API_KEY;
import static com.orevon.fflok.constants.ApiPaths.BASEURL;

public class LinkContactsActivity extends AppCompatActivity implements ContactRecordAdapter.AddFriendClickedCallBack,LinkContactPopupFragment.AddFriendUpdateCallBack{

    private static final int INITIALS_MAX_LEN = 2;
    private final int PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    private TextView subTitle;
    private ListView listViewContacts;
    private LoginButton fbLoginButton;

    private ArrayList<ContactRecord> contactRecords;
    private List<String> userFriendPhoneNumList = new ArrayList<>();
    private Cursor phones;
    private ContentResolver resolver;

    private CallbackManager callbackManager;
    ContactRecordAdapter contactRecordsAdapter;
    int processedContactCount = 0;
    private ProgressHandler progressHandler;
    int new_friend_count = 0;
    ContactRecord selectedFriendRecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link_contacts);

        Log.d("onCreate", "kollu");

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        initControls();
        requestShowContacts();
//        fetchUserFriends();
        findViewById(R.id.pageIndicator)
                .setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
//                                            if(new_friend_count == 0) {
                                                finish();
                                                Intent intent = new Intent(LinkContactsActivity.this, MainActivity.class);
                                                startActivity(intent);
//                                            } else {
//                                                LinkContactPopupFragment fr = LinkContactPopupFragment.newInstance();
//                                                fr.setContactCount(new_friend_count);
//                                                fr.setContactRecord(selectedFriendRecord);
//                                                fr.show(getFragmentManager(), "");
//                                            }

                                        }
                                    }
                );


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    finish();
//                    Intent intent = new Intent(LinkContactsChoiceActivity.this, InviteContactsActivity.class);
//                    startActivity(intent);
                    loadContacts();
                } else {
                    // Permission was not granted.
                    subTitle.setText(getString(R.string.link_contacts_msg3));
                }
            }
        }
    }

    private void initControls() {
        initTitle();
        subTitle = findViewById(R.id.subTitle);
        initSearch();
        initFBButton();
    }

    private void initFBButton() {
        fbLoginButton = findViewById(R.id.login_button);
        fbLoginButton.setReadPermissions("user_friends");
        fbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("onSuccess", loginResult.getAccessToken().getToken());
                Log.d("onSuccess", loginResult.getAccessToken().getUserId());
                GraphRequestAsyncTask graphRequestAsyncTask = new GraphRequest(
                        loginResult.getAccessToken(),
                        "/me/friends",
                        null,
                        HttpMethod.GET,
                        new GraphRequest.Callback() {
                            public void onCompleted(GraphResponse response) {
                                try {
                                    ArrayList<FBFriend> fbFriends = new ArrayList<>();
                                    JSONArray friendslist = response.getJSONObject().getJSONArray("data");
                                    for (int l=0; l < friendslist.length(); l++) {
                                        FBFriend fbFriend = new FBFriend();
                                        JSONObject friendObject = friendslist.getJSONObject(l);
                                        fbFriend.setName(friendObject.getString("name"));
                                        fbFriend.setId(friendObject.getString("id"));
                                        fbFriend.setProfilePicture(friendObject.getString("profile_pic"));
                                        fbFriends.add(fbFriend);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d("onCancel", "user canceled");
            }

            @Override
            public void onError(FacebookException e) {
                Log.e("onError", e.toString());
            }
        });
        findViewById(R.id.fbButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                AccessToken accessToken = AccessToken.getCurrentAccessToken();
//                if (accessToken == null) {
//                    fbLoginButton.performClick();
//                }
                fbLoginButton.performClick();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void initTitle() {
        TextView title = findViewById(R.id.title);
        String name = "Hey";
        if(FflokDataSingleton.getInstance().getUser() != null && FflokDataSingleton.getInstance().getUser().getUsername() != null && !FflokDataSingleton.getInstance().getUser().getUsername().getText().equals("")) {
            name = FflokDataSingleton.getInstance().getUser().getUsername().getText();
        }
        title.setText(String.format(title.getText().toString(), name));
    }

    private void requestShowContacts() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.READ_CONTACTS
                    },
                    PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {
            loadContacts();
        }
    }

    private void initSearch() {
        final EditText searchEditText = findViewById(R.id.nameSearchEditText);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/CircularStd-Book.otf");
        searchEditText.setTypeface(tf);
        searchEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = searchEditText.getText().toString().toLowerCase(Locale.getDefault());
                contactRecordsAdapter.filter(text);
                contactRecordsAdapter.notifyDataSetChanged();
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
                contactRecordsAdapter.notifyDataSetChanged();
            }
        });

        //Solution-2
        final View activityRootView = findViewById(R.id.link_contacts_layout);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                activityRootView.getWindowVisibleDisplayFrame(r);
                int screenHeight = activityRootView.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;

                Log.d("", "keypadHeight = " + keypadHeight);

                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    findViewById(R.id.top_layout).setVisibility(View.GONE);
                }
                else {
                    // keyboard is closed
                    findViewById(R.id.top_layout).setVisibility(View.VISIBLE);
                }
            }
        });
        searchEditText.clearFocus();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

//    private void fetchUserFriends() {
//        ProgressHandler progressHandler = new ProgressHandler(this);
//        StringRequest request = getUserFriendsVolleyRequest(progressHandler);
//        request.setShouldCache(false);
//        VolleySingleton.getInstance(getApplicationContext())
//                .addToRequestQueue(request);
//    }

//    private StringRequest getUserFriendsVolleyRequest(final ProgressHandler progressHandler) {
//        return new StringRequest(Request.Method.GET, BASEURL + GET_USER_FRIENDS,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.v("Volley:Response ", "" + response);
//                        UserFriendsResponse friendsResponse = new Gson().fromJson(response, UserFriendsResponse.class);
//                        userFriendPhoneNumList = new ArrayList<>();
//                        for(Friend friend: friendsResponse.getLikers()) {
////                            String name = friend.getId().getUsername().getText();
//                            String phone = friend.getId().getPhone();
//                            if(phone != "") {
//                                userFriendPhoneNumList.add(phone);
//                            }
//                        }
//                        progressHandler.hide();
//                        requestShowContacts();
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        progressHandler.hide();
//                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
//                        NetworkUtils.handleVolleyError(error, getApplicationContext());
//                    }
//                }
//        ) {
//            //
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("x-api-key", X_API_KEY);
//                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getApplicationContext()));
//                return headers;
//            }
//        };
//    }

    private StringRequest checkUsersVolleyRequest(final List<ContactRecord> contactRecordsSubList) {
        return new StringRequest(Request.Method.POST, BASEURL + CHECK_USERS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        processedContactCount += contactRecordsSubList.size();
                        Log.v("Volley:Response ", "" + response);
//                        progressHandler.hide();
                        setCheckUserFriendsResponse(response, contactRecordsSubList);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getApplicationContext());
                    }
                }
        ) {

//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                HashMap<String, String> params = new HashMap<String, String>();
//                params.put("listeUSers", contactsArray);
//                return params;
//            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getApplicationContext()));
//                headers.put("Content-Type", "application/json; charset=utf-8");
//                headers.put("Accept","application/json");
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                HashMap<String, Object> params = new HashMap<String, Object>();
                List<String> contacts = new ArrayList<>();
                for (ContactRecord record:
                        contactRecordsSubList) {
                    contacts.add(record.getPhone());
                }
//                String[] contactsArray = contacts.toArray(new String[contacts.size()]);
//                params.put("listeUSers", contactsArray);
                JSONArray json1 = new JSONArray(contacts);
                params.put("listeUsers", json1);
                return new JSONObject(params).toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

        };
    }

    private StringRequest addFriendVolleyRequest(final ProgressHandler progressHandler, final ContactRecord contactRecord) {
        return new StringRequest(Request.Method.PUT, BASEURL + ADD_FRIEND,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v("Volley:Response ", "" + response);
                        progressHandler.hide();
                        new_friend_count = new_friend_count + 1;
                        selectedFriendRecord = contactRecord;
                        contactRecords.get(contactRecords.indexOf(contactRecord)).setFflockerFriend(true);
                        contactRecordsAdapter.notifyDataSetChanged();
                        AddUserFriendResponse addUserFriendResponse = new Gson().fromJson(response, AddUserFriendResponse.class);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressHandler.hide();
                        Log.v("Volley:ERROR ", CommonUtils.getVolleyErrorMessage(error));
                        NetworkUtils.handleVolleyError(error, getApplicationContext());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("phone",contactRecord.getPhone().replaceAll("\\s+",""));
                String email = contactRecord.getEmail().replaceAll("\\s+","");
                params.put("email", CommonUtils.isValidEmail(email) ? email : "abc@gmail.com");
                params.put("name", contactRecord.getName());
                params.put("source", contactRecord.getSource());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("x-api-key", X_API_KEY);
                headers.put("Authorization", FflokDataSingleton.getInstance().getToken(getApplicationContext()));
                return headers;
            }
        };
    }

    private void loadContacts() {
        contactRecords = new ArrayList<>();
        resolver = this.getContentResolver();
        listViewContacts = findViewById(R.id.listViewContacts);
        String[] PROJECTION = new String[] { ContactsContract.RawContacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                ContactsContract.CommonDataKinds.Email.ADDRESS ,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_SOURCE};
        phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PROJECTION, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        progressHandler = new ProgressHandler(LinkContactsActivity.this);
        ContactLoaderTask contactLoader = new ContactLoaderTask();
        contactLoader.execute();
    }

    private void setCheckUserFriendsResponse(String response, List<ContactRecord> contactRecordsSubList) {
        try {
            JSONObject result = new JSONObject(response);
            JSONArray jsonarray = result.getJSONArray("listeUsers");
            ArrayList<UserContactFeature> userContactFeatures = new ArrayList<>();
            for(int i = 0; i < jsonarray.length(); i++) {
                UserContactFeature userContactFeature = new Gson().fromJson(jsonarray.get(i).toString(), UserContactFeature.class);
                userContactFeatures.add(userContactFeature);
            }

            for(ContactRecord record: contactRecordsSubList) {
                UserContactFeature feature = null;
                for (UserContactFeature ucf : userContactFeatures) {
                    if (ucf.getPhone().equalsIgnoreCase(record.getPhone())) {
                        feature = ucf;
                        break;
                    }
                }
                if(feature.getFriend()) {
                    record.setFflockerFriend(true);
                }
                if(feature.getFflok()) {
                    record.setBadgePresent(true);
                    record.setBadgePath(feature.getBadge());
                }

            }

            if(processedContactCount == contactRecords.size()) {
//                To sort the list in the order
//                      Contacts with images not ffloker friend
//                      Flockers
//                      Non-members
                List<ContactRecord> contactRecordsTemp = new ArrayList<>();
                for (ContactRecord record: contactRecords) {
                    contactRecordsTemp.add(record);
                }
                List<ContactRecord> contactRecordsThumbnailFriend = new ArrayList<>();
                List<ContactRecord> contactRecordsNoThumbnailFriend = new ArrayList<>();
                List<ContactRecord> contactRecordsNoThumbnailNoFriend = new ArrayList<>();

                contactRecords.clear();
                for (ContactRecord contactRecord : contactRecordsTemp) {
                    if (contactRecord.getThumbnail() != null && !contactRecord.isFflockerFriend()) {
                        contactRecords.add(contactRecord);
                    } else if(contactRecord.getThumbnail() != null && contactRecord.isFflockerFriend()) {
                        contactRecordsThumbnailFriend.add(contactRecord);
                    } else if(contactRecord.getThumbnail() == null && contactRecord.isFflockerFriend()) {
                        contactRecordsNoThumbnailFriend.add(contactRecord);
                    } else {
                        contactRecordsNoThumbnailNoFriend.add(contactRecord);
                    }
                }
                contactRecords.addAll(contactRecordsThumbnailFriend);
                contactRecords.addAll(contactRecordsNoThumbnailFriend);
                contactRecords.addAll(contactRecordsNoThumbnailNoFriend);
//                for (ContactRecord contactRecord : contactRecordsTemp) {
//                    if (contactRecord.getThumbnail() == null && contactRecord.isFflockerFriend()) {
//                        contactRecords.add(contactRecord);
//                    }
//                }
//                for (ContactRecord contactRecord : contactRecordsTemp) {
//                    if (!contactRecord.isFflockerFriend() && contactRecord.getThumbnail() == null) {
//                        contactRecords.add(contactRecord);
//                    }
//                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        contactRecordsAdapter.notifyDataSetChanged();
        if(processedContactCount == contactRecords.size()) {
            contactRecordsAdapter.setContactRecordList(contactRecords);
            progressHandler.hide();
        }
    }

    @Override
    public void addFriendClicked(int position, ContactRecord contactRecord) {
//        ProgressHandler progressHandler = new ProgressHandler(this);
//        StringRequest request = addFriendVolleyRequest(progressHandler, contactRecord);
//        request.setShouldCache(false);
//        VolleySingleton.getInstance(getApplicationContext())
//                .addToRequestQueue(request);

        LinkContactPopupFragment fr = LinkContactPopupFragment.newInstance();
        fr.setCallback(LinkContactsActivity.this);
        fr.setContactCount(new_friend_count);
        fr.setContactRecord(contactRecord);
        fr.show(getFragmentManager(), "");
    }

    @Override
    public void addFriendUpdated(ContactRecord contactRecord) {
        new_friend_count = new_friend_count + 1;
        selectedFriendRecord = contactRecord;
        contactRecords.get(contactRecords.indexOf(contactRecord)).setFflockerFriend(true);
        contactRecordsAdapter.notifyDataSetChanged();
    }

    // Load data on background
    class ContactLoaderTask extends AsyncTask<Void, Void, Void> {
        private String getInitials(String contactId, String displayName) {
            // TODO: Fix
            // For now get the initials from the displayName
            // Querying for given-name and family-name is taking awefully long
            /*
            try {
                String structuredNameWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
                String[] structuredNameWhereParams = new String[]{
                        contactId,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE
                };
                Cursor cursor = getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, structuredNameWhere, structuredNameWhereParams, null);
                if (cursor.moveToFirst()) {
                    String givenName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
                    String familyName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
                    StringBuilder initialsBuilder = new StringBuilder();
                    if (!givenName.isEmpty()) {
                        initialsBuilder.append(givenName.charAt(0));
                    }
                    if (!familyName.isEmpty()) {
                        initialsBuilder.append(familyName.charAt(0));
                    }
                    return initialsBuilder.toString().toUpperCase();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            */

            String[] parts = displayName.split(" ");
            StringBuilder initialsBuilder = new StringBuilder();
            for (String part : parts) {
                initialsBuilder.append(part.charAt(0));
                if (initialsBuilder.length() == INITIALS_MAX_LEN) {
                    break;
                }
            }

            return initialsBuilder.toString().toUpperCase();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Get Contact list from Phone
            Looper.prepare();
            if (phones != null) {
                Random rand = new Random();
                int phonesCount = phones.getCount();


                int fflockerCount = 8;
                int superFflockerCount = 4;
                ArrayList<ContactRecord> contactRecordsTemp = new ArrayList<>();

                while (phones.moveToNext()) {
                    --phonesCount;
                    Bitmap bitmapThumbnail = null;
                    String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String uriThumbnail = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));
                    String email = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                    String source = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_SOURCE));

                    try {
                        if (uriThumbnail != null) {
                            bitmapThumbnail = MediaStore.Images.Media.getBitmap(resolver, Uri.parse(uriThumbnail));
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String initials = null;
                    if (bitmapThumbnail == null) {
                        String contactId = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                        if ((contactId != null) && (!contactId.isEmpty())) {
                            initials = getInitials(contactId, name);
                            /* To convert initials to a bitmap
                            TODO: Revisit

                            if ((initials != null) && (!initials.isEmpty())) {
                                bitmapThumbnail = Bitmap.createBitmap(64, 64, Bitmap.Config.ARGB_8888);
                                Canvas c = new Canvas(bitmapThumbnail);
                                Paint paint = new Paint();
                                int xPos = (c.getWidth() / 2);
                                int yPos = (c.getHeight() / 2);
                                Rect bounds = new Rect();
                                paint.getTextBounds(initials, 0, initials.length(), bounds);
                                paint.setTextAlign(Paint.Align.CENTER);
                                c.drawText(initials, xPos, yPos + bounds.height() * 0.5f, paint);
                            }
                            */
                        }
                    }

                    ContactRecord contactRecord = new ContactRecord();
                    contactRecord.setThumb(bitmapThumbnail);
                    contactRecord.setName(name);
                    phoneNumber = phoneNumber.replaceAll("\\s+","");
                    contactRecord.setPhone(phoneNumber);
                    contactRecord.setInitials(initials);
                    String emailtext = email.replaceAll("\\s+","");
                    contactRecord.setEmail(CommonUtils.isValidEmail(emailtext) ? emailtext : "abc@gmail.com");
//                    contactRecord.setSource(source);
                    contactRecord.setSource("Phone");

//                    if (userFriendPhoneNumList.contains(phoneNumber)) {
//                        contactRecord.setFflocker(true);
//                    }

                    contactRecordsTemp.add(contactRecord);

                }

//                 To sort the list in the order
//                      Contacts with images
//                      Rest
                for (ContactRecord contactRecord : contactRecordsTemp) {
                    if (contactRecord.getThumbnail() != null) {
                        contactRecords.add(contactRecord);
                    }
                }
                for (ContactRecord contactRecord : contactRecordsTemp) {
                    if (contactRecord.getThumbnail() == null) {
                        contactRecords.add(contactRecord);
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            contactRecordsAdapter = new ContactRecordAdapter(contactRecords, LinkContactsActivity.this);
            contactRecordsAdapter.setCallback(LinkContactsActivity.this);
            listViewContacts.setAdapter(contactRecordsAdapter);

            listViewContacts.setFastScrollEnabled(true);
            ContactFeaturesLoaderTask contactFeaturesLoaderTask = new ContactFeaturesLoaderTask();
            contactFeaturesLoaderTask.execute();
        }
    }

    class ContactFeaturesLoaderTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            processedContactCount = 0;
            for(int i = 0; i< contactRecords.size(); i+=10) {
                List<ContactRecord> contactRecordsSubList = new ArrayList<>();
                if(i+10 < contactRecords.size()) {
                    contactRecordsSubList = contactRecords.subList(i, i+10);
                } else {
                    contactRecordsSubList = contactRecords.subList(i, contactRecords.size());
                }

                StringRequest request = checkUsersVolleyRequest(contactRecordsSubList);
                request.setShouldCache(false);
                VolleySingleton.getInstance(getApplicationContext())
                        .addToRequestQueue(request);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
//            contactRecordsAdapter.notifyDataSetChanged();
//            contactRecordsAdapter = new ContactRecordAdapter(contactRecords, LinkContactsActivity.this);
//            contactRecordsAdapter.setCallback(LinkContactsActivity.this);
//            listViewContacts.setAdapter(contactRecordsAdapter);
//
//            listViewContacts.setFastScrollEnabled(true);
//        }
    }
}
