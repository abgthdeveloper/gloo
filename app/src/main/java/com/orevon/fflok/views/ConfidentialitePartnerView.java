package com.orevon.fflok.views;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.orevon.fflok.R;
import com.orevon.fflok.models.DataPartner;
import com.orevon.fflok.models.DataShared;

import java.util.List;

public class ConfidentialitePartnerView extends LinearLayout {
    public LinearLayout parterLayout, dataLayout;
    private List<DataPartner> dataPartners;
    Context context;
    public ConfidentialitePartnerView(Context context) {
        super(context);
        initViews(context, null);
    }

    public ConfidentialitePartnerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context, attrs);
    }

    public ConfidentialitePartnerView(Context context, AttributeSet attrs, int defStyle) {
        this(context, attrs);
        initViews(context, attrs);
    }

    private void initViews(Context context, AttributeSet attrs) {
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.confidentialite_partner_view, this);
        parterLayout = (LinearLayout) this.findViewById(R.id.partnerLayout);
        dataLayout = (LinearLayout) this.findViewById(R.id.dataLayout);
    }

    public void setData(List<DataPartner> dataPartners) {
        this.dataPartners = dataPartners;
        arrangeDataPartners();
    }

    private void arrangeDataPartners() {
        dataLayout.removeAllViews();
        if (this.dataPartners.size() > 0 ){
            for (DataShared dataShared: this.dataPartners.get(0).getDataSharedList()) {
                TextView textView = new TextView(context);
                textView.setTextSize(10);
                textView.setGravity(Gravity.CENTER);
                textView.setText(dataShared.getName());
                textView.setLayoutParams(new LayoutParams(105, ViewGroup.LayoutParams.WRAP_CONTENT));
                dataLayout.addView(textView);
            }
        }


        for(DataPartner dataPartner: this.dataPartners) {
            LinearLayout parent = new LinearLayout(context);
            LinearLayout.LayoutParams lp =  new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            lp.setMargins(0,5,0,5);
            parent.setLayoutParams(lp);
            parent.setOrientation(LinearLayout.HORIZONTAL);

            ImageView imageView = new ImageView(context);
            imageView.setImageResource(dataPartner.getPartnerImageId());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(100, 60);
            layoutParams.setMargins(20,0,25,0);
            imageView.setLayoutParams(layoutParams);
            parent.addView(imageView);
            for(DataShared dataShared:dataPartner.getDataSharedList()) {
                RadioButton radioButton = new RadioButton(context);
                if(dataShared.isDataShared()) {
                    radioButton.setChecked(true);
                    radioButton.setAlpha(1);
                    radioButton.setButtonDrawable(getRadioButtonDrawableFor(dataShared.getColor()));
                } else {
                    radioButton.setChecked(true);
                    radioButton.setAlpha((float)0.5);
                    radioButton.setButtonDrawable(R.drawable.radio_on_profile_grey);
                }
                layoutParams = new LinearLayout.LayoutParams(40, 40);
                layoutParams.setMargins(0,10,0,10);
                radioButton.setLayoutParams(layoutParams);
                parent.addView(radioButton);

                if(parent.getChildCount() < (dataPartner.getDataSharedList().size()*2)) {
                    View view = new View(context);
                    layoutParams = new LinearLayout.LayoutParams(75, 3);
                    layoutParams.setMargins(-10,27,0,27);
                    view.setLayoutParams(layoutParams);
                    view.setBackgroundColor(getResources().getColor(R.color.lightgrey));
                    parent.addView(view);
                }


            }
            parterLayout.addView(parent);
        }

    }

    private int getRadioButtonDrawableFor(int color) {
        switch (color) {
            case R.color.pulse:
                return R.drawable.radio_on_profile_red;
            case R.color.nouvelle_green:
                return R.drawable.radio_on_profile_green;
            case R.color.blue:
                return R.drawable.radio_on_profile_blue;
        }
        return R.drawable.radio_on_profile_grey;
    }

}
