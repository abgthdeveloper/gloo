package com.orevon.fflok.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.orevon.fflok.R;

public class ConfidentialiteTonAttitudeView extends RelativeLayout {
    public LinearLayout layout1, layout2, layout3;
    public ImageButton bird1, bird2, bird3;
    public TextView textView1, textView2, textView3;
    public ImageView stepCompleted1, stepCompleted2, stepCompleted3;
    public View connector1, connector2;

    public OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.bird1:setLevelCompleted(1);
                    break;
                case R.id.bird2:setLevelCompleted(2);
                    break;
                case R.id.bird3:setLevelCompleted(3);
                    break;
            }
        }
    };

    public ConfidentialiteTonAttitudeView(Context context) {
        super(context);
        initViews(context, null);
    }

    public ConfidentialiteTonAttitudeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context, attrs);
    }

    public ConfidentialiteTonAttitudeView(Context context, AttributeSet attrs, int defStyle) {
        this(context, attrs);
        initViews(context, attrs);
    }

    private void initViews(Context context, AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.confidentialite_ton_attitude, this);
        layout1 = (LinearLayout) this.findViewById(R.id.linearLayout1);
        layout2 = (LinearLayout) this.findViewById(R.id.linearLayout2);
        layout3 = (LinearLayout) this.findViewById(R.id.linearLayout3);

        bird1 = (ImageButton) this.findViewById(R.id.bird1);
        bird2 = (ImageButton) this.findViewById(R.id.bird2);
        bird3 = (ImageButton) this.findViewById(R.id.bird3);

        bird1.setOnClickListener(onClickListener);
        bird2.setOnClickListener(onClickListener);
        bird3.setOnClickListener(onClickListener);

        textView1 = (TextView) this.findViewById(R.id.text1);
        textView2 = (TextView) this.findViewById(R.id.text2);
        textView3 = (TextView) this.findViewById(R.id.text3);

        stepCompleted1 = (ImageView) this.findViewById(R.id.stepCompleted1);
        stepCompleted2 = (ImageView) this.findViewById(R.id.stepCompleted2);
        stepCompleted3 = (ImageView) this.findViewById(R.id.stepCompleted3);

        connector1 = (View) this.findViewById(R.id.connector1);
        connector2 = (View) this.findViewById(R.id.connector2);
    }

    public void setLevelCompleted(int position) { //accepts 1,2,3
        switch (position) {
            case 1:
                layout1.setAlpha(1);
                layout2.setAlpha((float)0.4);
                layout3.setAlpha((float)0.4);
                connector1.setAlpha((float)0.4);
                connector2.setAlpha((float)0.4);
                break;
            case 2:
                layout1.setAlpha(1);
                layout2.setAlpha(1);
                layout3.setAlpha((float)0.4);
                connector1.setAlpha(1);
                connector2.setAlpha((float)0.4);
                break;
            case 3:
                layout1.setAlpha(1);
                layout2.setAlpha(1);
                layout3.setAlpha(1);
                connector1.setAlpha(1);
                connector2.setAlpha(1);
                break;
            default:
                break;
        }
    }



}