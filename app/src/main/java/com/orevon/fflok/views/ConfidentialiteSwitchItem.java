package com.orevon.fflok.views;

import android.content.Context;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.orevon.fflok.R;

public class ConfidentialiteSwitchItem extends LinearLayout {
    public TextView switchText;
    public
    SwitchCompat aSwitch;
    public ImageView switchItemImage;

    public ConfidentialiteSwitchItem(Context context) {
        super(context);
        initViews(context, null);
    }

    public ConfidentialiteSwitchItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context, attrs);
    }

    public ConfidentialiteSwitchItem(Context context, AttributeSet attrs, int defStyle) {
        this(context, attrs);
        initViews(context, attrs);
    }

    private void initViews(Context context, AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.confidentialite_switch_item, this);
        switchText = (TextView) this.findViewById(R.id.switch_text);
        aSwitch = (SwitchCompat) this.findViewById(R.id.switch_item);
        switchItemImage = (ImageView) this.findViewById(R.id.switch_item_image);
    }
}