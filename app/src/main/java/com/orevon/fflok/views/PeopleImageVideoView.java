package com.orevon.fflok.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.orevon.fflok.R;
import com.orevon.fflok.models.Friend;
import com.orevon.fflok.models.ProximateFriend;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.orevon.fflok.constants.ApiPaths.BASE_VIDEO_URL;

public class PeopleImageVideoView extends RelativeLayout implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener {
    private Context mContext;
    CircleSurface surface;
    MediaPlayer player;
    public CircleImageView profileImageView;
    ImageView profileSmallIconImageView;
    RelativeLayout rootView;
    SurfaceHolder holder;
    String videoPath;
//    ProgressHandler progressHandler;
    ProximateFriend proximateFriend;
    PeopleImageVideoViewClickedCallBack callback;

    public PeopleImageVideoView(Context context) {
        super(context);
        initialize(context, null);
    }

    public PeopleImageVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context, attrs);
    }

    public PeopleImageVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context, attrs);
    }

    private void initialize(Context context, final AttributeSet attrs) {
        mContext = context;
        LayoutInflater.from(mContext).inflate(R.layout.people_image_video_view, this);
        TypedArray typedArray = null;
        if (attrs != null) {
            typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.PeopleImageVideoView, 0, 0);
        }
        rootView = (RelativeLayout) findViewById(R.id.people_image_video_root);
        final TypedArray finalTypedArray = typedArray;
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (rootView.getHeight() > 0) {
                    rootView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    surface = new CircleSurface(mContext, rootView.getHeight() / 2, rootView.getHeight() / 2, rootView.getHeight() / 2);
                    surface.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                    addView(surface);
                    surface.setVisibility(GONE);
                    holder = surface.getHolder();
                    holder.addCallback(PeopleImageVideoView.this);

                    if (attrs != null && finalTypedArray != null) {
                        if (finalTypedArray.getResourceId(R.styleable.PeopleImageVideoView_video, 0) != 0) {
                            player = MediaPlayer.create(mContext, finalTypedArray.getResourceId(R.styleable.PeopleImageVideoView_video, 0));
                            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    mp.stop();
                                    mp.reset();
                                    player = MediaPlayer.create(mContext, finalTypedArray.getResourceId(R.styleable.PeopleImageVideoView_video, 0));
                                    try {
                                        if(holder != null && player != null) {
                                            player.setDisplay(holder);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    player.setOnCompletionListener(this);
                                    profileImageView.setVisibility(VISIBLE);
                                    surface.setVisibility(GONE);
                                    profileImageView.clearColorFilter();
                                }
                            });
                        }
                    }
                }
            }
        });
        profileImageView = (CircleImageView) findViewById(R.id.profile_image);
        if (attrs != null && typedArray != null) {
            profileImageView.setImageDrawable(ContextCompat.getDrawable(mContext, typedArray.getResourceId(R.styleable.PeopleImageVideoView_image, 0)));
        }
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        profileImageView.setColorFilter(filter);
        profileImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                playVideo();
                if(callback != null && proximateFriend != null) {
                    callback.peopleImageVideoViewClicked(proximateFriend);
                }
            }
        });
    }

    public void playVideo() {
        if (player != null) {
            profileImageView.setVisibility(GONE);
            surface.setVisibility(VISIBLE);
            player.start();
        } else {
            profileImageView.clearColorFilter();
        }
    }

    public void setProximateFriend(ProximateFriend friend) {
        this.proximateFriend = friend;
    }

    public void setData(final String videoUrl, Boolean isColorFilterRequired) {
        videoPath = videoUrl;
        Log.e("videopath1",videoPath);
        if(isColorFilterRequired) {
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            profileImageView.setColorFilter(filter);
        } else {
            profileImageView.clearColorFilter();
        }

        player = new MediaPlayer();
        try {
            player.setDataSource(videoPath);
//            progressHandler = new ProgressHandler(getContext());
            player.prepareAsync();

        } catch (IOException e) {
//            progressHandler.hide();
            e.printStackTrace();
        }
        player.setOnPreparedListener(PeopleImageVideoView.this);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);

        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.pause();
                profileImageView.setVisibility(VISIBLE);
                surface.setVisibility(GONE);
                profileImageView.clearColorFilter();
            }
        });

        profileImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                profileImageView.setVisibility(GONE);
                surface.setVisibility(VISIBLE);
                player.start();
                setHighlight(true);

            }
        });
    }

    public void setHighlight(Boolean setHighlight) {
        CircleImageView highlightView = (CircleImageView) findViewById(R.id.highlight);
        if(setHighlight) {
            highlightView.setVisibility(View.VISIBLE);
        } else {
            highlightView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
//        progressHandler.hide();
        Log.e("videopath2",videoPath);
        if(videoPath != BASE_VIDEO_URL && videoPath != null && videoPath != "") {
            Glide.with(getContext())
                    .load(videoPath) // Uri of the picture
                    .apply(RequestOptions.circleCropTransform())
                    .into(profileImageView);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        player.setDisplay(holder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
//        player.setDisplay(null);
    }

    public void setCallback(PeopleImageVideoViewClickedCallBack callback){
        this.callback = callback;
    }

    public interface PeopleImageVideoViewClickedCallBack{
        public void peopleImageVideoViewClicked(ProximateFriend friend);
    }
}
